export var customscript = function() {

    $('.banner_slide').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    })

    $('body').on('click', '.rateNow', function() {
        var value = $(this).val();
        $('.rateing-item').hide();
        $('.smile-' + value).show();
    });


}