import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StringifyOptions } from 'querystring';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import {TranslateService} from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateHorizontalPosition } from '@angular/cdk/overlay';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  name:any;
  data:any;
  cname:string = null;
  cemail:string = null;
  submitted:boolean = false;
  cdescription:string = null;
  slugname:any;
  title:any;
  description:any;
  profileForm = new FormGroup({
    name: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    description: new FormControl('',Validators.required)
  });
  language: any;
  constructor(private route:ActivatedRoute,private api :AuthenticationService ,private translate:TranslateService,private router:Router,private commanSrv:CommonService) {
    this.api.languagechange.subscribe((res:any)=>
    {
      this.language = res;
      
      console.log(res);
    })
  }

  ngOnInit() {

    this.language = localStorage.getItem('lan')
    this.route.queryParams.subscribe(params => {this.name = params['check'],
    this.title = params['name']
  });
  console.log(this.title)
   this.gettermsPolicy(this.name)
   this.api.$termscond.subscribe((res)=>
   {
     this.gettermsPolicy(res);
   })
  }

  public errorHandling = (control: string, error: string) => {
    return this.profileForm.controls[control].hasError(error);
  }


  gettermsPolicy(value)
  {
    this.api.gettermsandpolicy().subscribe((res)=>{
      this.data = res.data.filter(id=>id.slugName == value)
      this.slugname = this.language == 'en' ? this.title == undefined ? this.data[0].title : 'Help' : this.title == undefined ? this.data[0].title : 'مساعدة'
      this.description = this.data[0].description;
      console.log(this.data)
    })
  }

  sendContactUs()
  {
    this.submitted = true;
   const data =
   {
   "name":this.cname,
   "email":this.cemail,
   "message":this.cdescription,
   } 
  console.log(this.profileForm.valid);
  if(this.profileForm.valid)
  {
    this.api.contactUs(data).subscribe((res:any)=>
    {
     if(res.success)
     {
      this.submitted = false;
       this.commanSrv.showToasterSuccess("Thanks for Contacting Us!")
       this.profileForm.reset();
     }
    })
  }
  else
  {
    //this.commanSrv.showToasterError("Please fill all the mandatory(*) fields")
  }
  }

}
