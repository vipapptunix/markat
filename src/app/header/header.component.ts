import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogboxComponent } from '../dialogbox/dialogbox.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AuthenticationService } from '../services/auth.service';
import { NotificationService } from '../services/notification.service';
import { TranslateService } from "@ngx-translate/core";
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as js from '../../assets/js/custom';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  authToken: any;
  isloggin:any;
  bannerData:any;
  categoryData:any;
  public homeData:any;
  public count:any;
  profiledata:any;
  searchtxt:any;
  searchvalue:any;
  googleid:any;
  firstname:any;
  cartdata:any;
  googlevalue:any;
  countcartlen:any;
  lang:any;
  countcart:any;
  categoryDataVal:any;
  search_result:any;
  bannerlist:any;
  subcategeorylist:any;
  listsubcategory:any;
  categoryid:any;
  sublength:any;
  imgurl:any;
  language:any='en';
  profilepicdata:any;
  @Output() searchtextvalue = new EventEmitter();
  imgflag: string;
  constructor(private router: Router,private route: ActivatedRoute,private dialog: MatDialog,private translate:TranslateService,
    public matDialog: MatDialog,private authservice:AuthenticationService, private notify:NotificationService) 
    { 
    translate.addLangs(["en", "arb"]);
    translate.setDefaultLang('en');

  this.authservice.googlefbdata.subscribe((res:any)=>
  {
    this.googleid = res.id;
   
  })
   this.authToken = JSON.parse(localStorage.getItem('authToken'));
   this.isloggin = localStorage.getItem("sucess");

  this.authservice.$profilepic_check.subscribe((res:any)=>
  {
    this.profilepicdata = res.profilePic;
    this.firstname = res.firstName;
    
  })
  this.imgurl=this.authservice.imageUrl;
 // this.notify.setupSocketConnection();
 //this.route.queryParams.subscribe(params => {this.language = params['lan']});
this.language = localStorage.getItem('lan')
this.imgflag = this.language == 'en' ?  'assets/images/eng_flag.jpg' : 'assets/images/arb_flag.jpg' ;
  }

  ngOnInit() {
    this.translate.setDefaultLang(this.language);
//  this.authservice.languagechange.subscribe((res:any)=>
//  {
//    this.language = res;
   
//    console.log(res);
//  })
   
    if(localStorage.getItem('provider') == 'GOOGLE' || localStorage.getItem('provider') == 'FACEBOOK')
    {
     
      this.authservice.Userdata.subscribe(userdata => {
        if(userdata.success)
        {
          this.firstname = userdata.data.firstName;
        }
        
        });
    }
   

   // console.log(":",this.isloggin);
    if(localStorage.getItem("sucess"))
    {
      this.authservice.getprofile().subscribe((profdata: any) => {
    
      //console.log("1");
      this.profilepicdata = profdata.data.profilePic;
       this.profiledata = profdata.data._id;
       this.firstname = profdata.data.firstName;
     
     });

    
   this.authservice.cartlen.subscribe((res:any)=>
   {
    this.countcartlen = res;
     this.count = this.countcartlen;
   })

   this.authservice.homeprofname.subscribe((res:any)=>
   {
     //when profile page setup it set value to my account.
   })

    }
  
    this.getDashboard();

  if(localStorage.getItem('sucess'))
  {
    // this.authservice.getcartlist().subscribe((res:any)=>{
    //   this.cartdata = res.data;
    //   this.getcartcount(res.data.length);
    // }); 
  }
     
  if(this.isloggin == null && JSON.parse(localStorage.getItem('cartcount')) == undefined ? '' : JSON.parse(localStorage.getItem('cartcount')).length > 0)
  {
    this.countcartlen = JSON.parse(localStorage.getItem('cartcount')).length
  }
  js.customscript();
  }

  languageChange(lan)
  {
    this.translate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
    if(this.lang == 'en') this.imgflag = 'assets/images/eng_flag.jpg';
    if(this.lang == 'arb') this.imgflag = 'assets/images/arb_flag.jpg'
    this.authservice.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }
  
  logindirect()
  {
    this.router.navigate(['/login'])
  }

  getDashboard()
  {
   this.authservice.cartlen.subscribe((res:any)=>
   {
    //  console.log("getcartlist",res);
  //console.log("4");
    this.countcartlen = res;
     this.count = this.countcartlen;
   })

  //   this.authservice.getDashboard().subscribe((res:any)=>{
 
  //   if(res.success){
  
  // this.homeData=res.data.output;
  
  // this.categoryData=this.homeData.filter(ij=>ij.label=="Shop By Category")[0];
  
  // if(this.categoryData!==null && this.categoryData!==undefined){
  //   this.categoryDataVal=this.categoryData.value;
     
  //   }
  // }
  // });
}

Help()
{
  this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
}

getcategoryId(id,i)
{
  if(i == 1)
  {
    this.categoryid = id;
    this.authservice.getcategoryTablist(id).subscribe((res:any)=>
    {
      this.bannerlist = res.data[1].value;
      this.subcategeorylist = res.data[2].value;
      this.sublength = this.subcategeorylist ;
      this.listsubcategory = res.data[3].value;
     // this.categorylist = this.categorylist[0].filter(id=>id.label == 'Categories')
    })
  }
  if(i == 2)
  {
    const data =
    {
      "id":id,
       "catid":this.categoryid
    }
    this.authservice.checkproductId(data);
    //categories?id=5f9bc261d522c0104cb06edf&catid=5eb9158aa49e9d6d7a98886b&name=subproduct&prod=Category
    this.router.navigate(['/categories'],{queryParams:{'prod':'Category','name':'subproduct','id':id,'catid':this.categoryid}})
  }  
}


  goTocart() {
 
      this.router.navigate(['cart']);
  }
  goTohome() {
    this.router.navigate(['/home']);

  }

  headerpage(id)
  {
      this.authservice.tranferhistoryid(id);
    
    this.router.navigate(['/headerpage']);
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

  goTocheckout() {
    this.router.navigate(['/checkout']);
  }
//Addacoount
  goToprofile() {
    if(this.isloggin)
    {
      this.router.navigate(['/profile']);
    }
    else
    {
     // this.openModal()
    }
  }


  goTowishlist() {
    this.router.navigate(['/wishlist']);
  }

  goTomyProfile(value)
  {
    if(value == 1)
    {
      this.router.navigate(['/login']);  
    }
    if(value == 2)
    {
      this.router.navigate(['/signup']);
    }

  }

  getcartcount(count)
  {
   // console.log(count);
    this.countcart = 0;
    for(var i=0;i<count;i++)
    {
          this.countcart =  this.cartdata[i].quantity + this.countcart;
      //  console.log("co",this.countcart)
    }
    this.authservice.getcartlength(this.countcart);

  }

  searchtext(e)
  {
    this.searchvalue = e.target.value;
    this.searchtextvalue.emit(this.searchvalue);
    this.searchtextvalue.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(value => {
        this.searchvalue = value;
        this.router.navigate(['/categories'], { queryParams: { 'Sval': this.searchvalue ,'result':'search'}});
      });
  }

  openmodal()
  {
    document.getElementById('openmodal').click()
  }

  movetocategories()
  {
  
    if(this.searchvalue != null)
    {
    this.authservice.searchdata(this.searchvalue);
      // this.router.navigate(['/categories']);
      const data = 
      {
        "limit":10,
            "skip":0,
            "searchText": this.searchvalue,
            "userId":"5eb3d95660b4e137e4e0934b"
      }
      this.authservice.searchproduct(data).subscribe((res:any)=>{
    
        if(res.data.length == 0)
        {
          this.router.navigate(['/notfound']);
        }
        else{
          this.router.navigate(['/categories']);
        }
       });
    }
  }

  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }


}
