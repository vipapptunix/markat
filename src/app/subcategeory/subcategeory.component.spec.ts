import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcategeoryComponent } from './subcategeory.component';

describe('SubcategeoryComponent', () => {
  let component: SubcategeoryComponent;
  let fixture: ComponentFixture<SubcategeoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubcategeoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcategeoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
