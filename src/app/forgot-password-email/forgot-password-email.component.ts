import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from '../services/auth.service';
import { TranslateService } from "@ngx-translate/core";
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-forgot-password-email',
  templateUrl: './forgot-password-email.component.html',
  styleUrls: ['./forgot-password-email.component.scss']
})
export class ForgotPasswordEmailComponent implements OnInit {
  submitted:boolean=false;
  ProfileForm: FormGroup;
  language: any;
  constructor(private fb: FormBuilder,
    private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private _route: ActivatedRoute,private translate:TranslateService) {

      this.authenticationService.languagechange.subscribe((res:any)=>
      {
        this.language = res
      })
     }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.submitted=false;
 
    this.createForm();
    
  }
  public errorHandling = (control: string, error: string) => {
    return this.ProfileForm.controls[control].hasError(error);
  }
  createForm() {
    
        this.ProfileForm = this.fb.group({
         
          // userName: ["", Validators.required],
          email: ["", [Validators.required, Validators.email]]
      
        });
      }

      onSumbit() {
        const controls = this.ProfileForm.controls;
        this.submitted=true;
        /** check form */
        if (this.ProfileForm.invalid) {
          // this.commonService.showToasterError('Please fill required Field appropriately');
          Object.keys(controls).forEach(controlName =>
            controls[controlName].markAsTouched()
          );
          return;
          
        } else {
  
    
          this.prepareForm();
     
         
       
      }


    }




    prepareForm() {
      const controls = this.ProfileForm.controls;
  
  
  const userId = 'user001';
            // this.messagingService.requestPermission(userId);
            // this.messagingService.receiveMessage();
            // this.message = this.messagingService.currentMessage;
      let data = {
       
        "email": controls.email.value,
        
       
      };
  
      let formData = new FormData();
   

      formData.append("email", controls.email.value);

  
      this.completeSignup(formData,data);
    }
  
    completeSignup(_data,dataemail) {
      this.authenticationService.forgetpassword(dataemail).subscribe(
        (_response: any) => {
          console.log("Response is ",_response);
          if(_response.success){
            this.commonService.showToasterSuccess("Please Check your Mail");
        
             this.router.navigate(['/login']);
            // this.router.navigate(['verfication'], { queryParams: { 'email': dataemail.email }});
          }else
          {
            this.commonService.showToasterError(_response.message);
          }
          // else{
          //   this.commonService.showToasterError("Server Error");
          // }
          // if (_response.response.success) {
          //   localStorage.setItem(
          //     "token","SEC "+
          //     _response["data"]['authToken']
          //   );
          //   localStorage.setItem(
          //     "loggedInUser",JSON.stringify(
          //     _response["data"])
          //   );
          //   localStorage.removeItem("signUp");
          //   localStorage.removeItem("profileSetup");
  
          //   // this.router.navigate(["/"]);
          //   if(this.viewIs=="home"){
          //     this.router.navigate(["/"]);
          //   }
          //   else{
          //     this.router.navigate(["/home"]);
          //   }
            
          // } else {
          //   this.commonService.showToasterError(_response.response["message"]);
          // }
        },
        err => {
  
          console.log("error occured"+JSON.stringify(err));
          if (err.error)
            this.commonService.showToasterError(err.error["message"]);
        }
      );
    }


}
