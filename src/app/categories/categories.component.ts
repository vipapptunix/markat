import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { Options } from 'ng5-slider';
import { JsonPipe, Location } from '@angular/common';
import { from } from 'rxjs';
import { max, filter } from 'rxjs/operators';
import { TranslateService } from "@ngx-translate/core";
import { A11yModule } from '@angular/cdk/a11y';
import { PlatformLocation } from '@angular/common'
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  index = 0
  id: any = "";
  subCategeory: any = "";
  ratval: any;
  currentPage: number = 1;
  search_result: any;
  searchvalue: any;
  search: any;
  brandname: any;
  imgurl: any;
  subproduct: boolean = false;
  searchlist: boolean = false;
  categorylist: any;
  bannerlist: any;
  searchresulthide: boolean = false;
  listsubcategory: any;
  subcategeorylist: any;
  categorycartlist: any;
  categoryid: any;
  productdetail: any;
  fixedrate: any;
  length: any;
  realid: any;
  pagehide: boolean = false;
  realcategory: any;
  row: boolean = true;
  pricehide: boolean = true;
  brandarr: any = [];
  value: number = 0;
  highValue: number = 6000;
  options: Options = {
    floor: 0,
    ceil: 5000,
    enforceStep: false,
    enforceRange: false,
  };
  maxvalue: any;
  isfeature: boolean = true;
  bannerlist_id: any;
  name: any;
  BrandIdArr: any;
  bannerlist_type: any;
  prod: any;
  namefeat: string;
  bannerlist_list: any;
  brandlist: any = [];
  subcatnumb: any;
  numcheck: number;
  offerlist: any;
  sort = 1;
  catid: any;
  radiocheck: any;
  checkstatus: any;
  offertype: any;
  number: any;
  brandarrylist: any = [];
  userid: string;
  type: any;
  indexval: number;
  checkout: any;
  SeachVal: any;
  searchResult: any;
  isloggin: string;
  column: string = 'rowcat';
  subid: any;
  language: string;
  lang: any;
  constructor(private router: Router, private route: ActivatedRoute,private translate:TranslateService, public location: Location, public localcheck: PlatformLocation, public authenticationService: AuthenticationService) 
  {
   // this.language = localStorage.getItem('lan')
    this.authenticationService.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })
    this.imgurl = this.authenticationService.imageUrl;
    this.isloggin = localStorage.getItem("sucess");
    this.authenticationService.getDashboard().subscribe((res: any) => 
    {
      this.categorylist = res.data.output[1].value;
    });

    localcheck.onPopState(() => 
    {
      this.route.queryParams.subscribe(params => {
        this.prod = params['prod']
        this.id = params['id']
        this.name = params['name']
        this.namefeat = params['type']
        this.catid = params['catid']
        this.ratval = params['rating']
        this.brandarrylist = params['cat'] ? params['cat'].split(",") : []

        setTimeout(() => {
          window.location.reload();
        }, 500);
      });

      this.userid = localStorage.getItem('userid')
      console.log("checkrouting", this.brandarrylist);

    });
    this.language = localStorage.getItem('lan')
    this.authenticationService.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })
  }

  sortSelected(e, size, page) 
  {
    this.checkstatus = e.target.value;
    if (this.name != 'moretoexplore' && this.prod != 'Feature' && this.name != 'subproduct' && this.name != 'UpperCat') 
    {
      this.numcheck = 3;
      const bandata =
      {
        "count": size ? size : '10',
        "page": page ? page : '1',
        "id": this.bannerlist_id,
        "list": this.bannerlist_list,
        "type": this.bannerlist_type,
        "sortBy": e.target.value,
        "minPrice": this.value,
        "maxPrice": this.highValue,
        "brands": this.brandarr.length > 0 ? this.brandarr : '',
        "rating": this.radiocheck
      }

      this.authenticationService.getbannerprolist(bandata).subscribe((res) => {
        this.length = res.total;
        this.search_result = res.data;
        this.pagehide = false;
        if (res.status) {
          this.pricehide = true;
          this.productdetail = res.data;
          this.subproduct = true;
          this.searchlist = false;
          this.searchresulthide = true;
        }
      })
    }
    if (this.name == 'moretoexplore' || this.prod == 'Feature' || this.name == 'subproduct' || this.name == 'UpperCat') 
    {
      const data =
      {
        "id": localStorage.getItem('userid'),
        "count": size ? size : '10',
        "page": page ? page : '1',
        "category": this.prod == 'Feature'  ? "" : this.categoryid,
        "minPrice": this.value.toString(),
        "maxPrice": this.highValue.toString(),
        "sortBy": e.target.value,
        "brands": this.brandarr.length > 0 ? this.brandarr : '',
        "rating": this.radiocheck,
        "categories": this.prod == 'Feature' || this.name == 'moretoexplore' ? "" : [this.realcategory],                         
        "subCategories": this.prod == 'Feature' || this.name == 'moretoexplore' ? "" : [this.realid],  
        "subCategory":this.prod == 'Feature' ? this.realcategory : ''
      
      }


      this.authenticationService.getprouctdata(data).subscribe((res: any) => 
      {
        console.log("6")
        this.length = res.total;
        this.search_result = res.data;
        this.pagehide = false;
        if (res.status) {
          this.pricehide = true;
          this.productdetail = res.data;
          this.subproduct = true;
          this.searchlist = false;
          this.searchresulthide = true;
        }
      })
    }
  }
  ngOnInit() 
  {
    this.language = localStorage.getItem('lan')
    console.log("check-->", location)
    this.getvalueBrand()
    this.route.queryParams.subscribe(params => {
      this.prod = params['prod']
      this.brandarrylist = params['cat'] ? params['cat'].split(",") : []
      this.brandarr = this.brandarrylist;
    });
    this.route.queryParams.subscribe(params => {
      this.id = params['id']
      this.name = params['name']
      this.namefeat = params['type']
      this.catid = params['catid']
      this.SeachVal = params['Sval']
      this.searchResult = params['result']
    });

    this.userid = localStorage.getItem('userid')
    this.route.queryParams.subscribe(params => { this.ratval = params['rating'] })
    this.authenticationService.$searchvalue.subscribe((res: any) => {
    this.searchvalue = res;
    // console.log(this.searchvalue)
    })

    this.authenticationService.$bannerlist.subscribe((data: any) => {
      this.type = data.type;
      if (data.length != 0 && data.type != 'Feature' && this.name != 'moretoexplore') {
        console.log("1")
        this.offerlist = data.offer.list
        this.offertype = data.offer.type
        this.getbannnerdatalist(data, 1)
      }
      if (this.type == 'bannerlist' && this.name != 'moretoexplore') {
        console.log("2")
        this.bannerlist_id = data._id
        this.isfeature = true;
        this.getbannnerdatalist(data, 1)
      }
    })
    if (this.prod == 'bannerlist') this.subcatnumb = 1;
    if (this.name == 'moretoexplore') {
      console.log("3")
      this.realid = this.id
      this.getmoretoExplore()
    }
    if (this.name == 'subproduct') 
    {console.log("4")
      const res = { 'id': this.id, 'catid': this.catid }
      this.headerProductCat(res)
      this.categoryid = this.catid;
      this.authenticationService.$checkproductid.subscribe((res: any) => {
        console.log("check-product",res.length)
        if(res.length != 0)this.headerProductCat(res)
              });
    }
    if (this.name != 'moretoexplore' && this.namefeat != 'Feature' && this.name != 'subproduct' && this.prod != 'bannerlist' && this.searchResult != 'search' && this.name != 'UpperCat' && this.prod != 'Category') {
      console.log("5")
      this.authenticationService.$checkproductid.subscribe((res: any) => {
        res.catid = this.id
        this.realid = res.id
        this.getFeatureProduct(res)
      });
    }
    this.authenticationService.$footer.subscribe((data: any) => 
    {
      if (data == 'footer') 
      {
        this.route.queryParams.subscribe(params => { this.id = params['id']
        this.catid = params['catid'] });
         
        const res = { 'id': this.id, 'catid': this.catid }
        this.headerproductcategory(res)
      }
    })

    this.getBrandproduct()
    if(this.searchResult == 'search')
    {
      this.getsearchdata(this.SeachVal);
    }
      // if(this.name == 'UpperCat') 
      // {
      //   this.route.queryParams.subscribe(params => { this.id = params['categoryId'] 
      //   this.catid = params['catid'] 
      // });
      //   const res = { 'id': this.id, 'catid': this.catid }
      //   this.headerproductcategory(res)
      //   this.authenticationService.$checkproductid.subscribe((res: any) => {
      //         this.headerproductcategory(res)
      //       });
      // }   
  }


  getBrandproduct() 
  {
    const data = 
    {
      "page": '1',
      "count": '10',
      "search": this.search,
      "category":this.catid
    }

    this.authenticationService.getallbrand(data).subscribe((res: any) => {
      this.brandname = res.data;
      this.brandlist = this.brandname;
    })
  }

  //Get all brand
  checkBrand() {
    this.brandlist = this.brandname.filter(it => new RegExp(this.search, "i").test(it.name));
  }

  getvalueBrand() {
  }


  clearBrand() {
    // this.router.navigate([],{relativeTo:this.route,queryParams:{'prod':this.prod,'name':this.name,'id':this.id}})
    this.queryObj['prod'] = this.prod;
    this.queryObj['name'] = this.name;
    this.queryObj['id'] = this.id;
    this.queryObj['catid'] = this.catid
    delete this.queryObj['cat']
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    setTimeout(() => {
      this.clearRating();
    }, 500);
   
  }

  clearRating() {
    this.queryObj['prod'] = this.prod;
    this.queryObj['name'] = this.name;
    this.queryObj['id'] = this.id;
    this.queryObj['catid'] = this.catid
    this.queryObj['cat'] = this.brandarrylist.join(",");
    delete this.queryObj['rating']
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    setTimeout(() => {
      window.location.reload();
    }, 500);
  }


  //get all the feature data
  getfeaturedata(data) {
    //start from here
  }

  //ng build --href-base /mokabookweb/

  getmoretoExplore() {
    console.log("getmoretoExplore")
    this.realid = this.userid
    this.categoryid = this.id
    this.subcatnumb = 2;
    const data = { "id": this.userid, "count": '10', "page": '1', "category": this.id, "brands": this.brandarr.length > 0 ? this.brandarr : '', "rating": this.radiocheck }

    this.authenticationService.getprouctdata(data).subscribe((res: any) => {
      this.length = res.total;
      this.search_result = res.data;

      this.pagehide = false;
      if (res.status) {
        this.pricehide = true;
        this.productdetail = res.data;
        this.subproduct = true;
        this.searchlist = false;
        this.searchresulthide = true;
      }
    })
  }

  queryObj = {}
  filterBrancd(i, e, index) 
  {
    // this.brandarrylist.push(i);
    this.queryObj['prod'] = this.prod;
    this.queryObj['name'] = this.name;
    this.queryObj['id'] = this.id;
    this.queryObj['catid'] = this.catid
    this.queryObj['cat'] = this.brandarrylist.join(",");
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    if (e.checked) 
    {
      this.brandarrylist.push(i);
      this.queryObj['prod'] = this.prod;
      this.queryObj['name'] = this.name;
      this.queryObj['id'] = this.id;
      this.queryObj['catid'] = this.catid;
      this.queryObj['cat'] = this.brandarrylist.join(",");
      this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
      this.brandarr.push(i)
      if (this.name == 'moretoexplore') {
        this.getmoretoExplore()
      }
      else {
        const res = { 'id': this.id, 'catid': this.catid }
        this.headerproductcategory(res)
      }
    }
    if (!e.checked) 
    {
      this.brandarrylist = this.brandarrylist.filter(brand => brand != i)
      this.queryObj['cat'] = this.brandarrylist.join(",");

      this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
      this.indexval = this.brandarr.indexOf(i);
      this.brandarr.splice(this.indexval, 1)
      if (this.name == 'moretoexplore') {
        this.getmoretoExplore()
      }
      else {
        const res = { 'id': this.id, 'catid': this.catid }
        this.headerproductcategory(res)
      }
    }
    //  this.router.navigate(['/categories'],{queryParams: {'brand':'checked'}})
  }

  //rating product
  radioChange(e) 
  {
    this.queryObj['prod'] = this.prod;
    this.queryObj['name'] = this.name;
    this.queryObj['id'] = this.id;
    this.queryObj['catid'] = this.catid
    this.ratval = e.value;
    this.queryObj['rating'] = e.value
    this.router.navigate([], { queryParams: this.queryObj })
    this.radiocheck = e.value;
    if (this.name == 'moretoexplore') 
    {
      this.getmoretoExplore()
    }
    else 
    {
      const res = { 'id': this.id, 'catid': this.catid }
      this.headerproductcategory(res)
    }
    // this.router.navigate(['/categories'],{queryParams:{'rating':e.value}})
  }

  sliderEvent(page, size, checknum) 
  {
    if (this.subcatnumb == 1) 
    {
      const bandata =
      {
        "count": size ? size : '10',
        "page": page ? page : '1',
        "id": this.bannerlist_id,
        "list": this.bannerlist_list,
        "type": this.bannerlist_type,
        "sortBy": this.checkstatus ? this.checkstatus : 1,
        "minPrice": this.value,
        "maxPrice": this.highValue,
        "brands": this.brandarr.length > 0 ? this.brandarr : '',
        "rating": this.radiocheck
      }

      this.authenticationService.getbannerprolist(bandata).subscribe((res) => {
        this.length = res.total;
        this.search_result = res.data;
        this.pagehide = false;
        if (res.status) {
          this.pricehide = true;
          this.productdetail = res.data;
          this.subproduct = true;
          this.searchlist = false;
          this.searchresulthide = true;
        }
      })
    }
    if (this.subcatnumb == 2) {
      this.realcategory = this.categoryid;
      if(this.name == 'subproduct')
      {
        this.categoryid = this.catid
        this.subid = this.id;
      }
      const data =
      {
        "id": localStorage.getItem('userid'),
        "count": size ? size : '10',
        "page": page ? page : '1',
        "category":this.prod == 'Feature' ? "" :  this.categoryid,
        "minPrice": this.value.toString(),
        "maxPrice": this.highValue.toString(),
        "sortBy": this.checkstatus ? this.checkstatus : 1,
        "brands": this.brandarr.length > 0 ? this.brandarr : '',
        "rating": this.radiocheck,
        "isRecommended": false,  
        "isFeatured": false,           
        "categories":this.prod == 'Feature' ? "" :  [this.categoryid],
        "subCategories":this.prod == 'Feature' ? "" : [this.subid],
        "subCategory":this.prod == 'Feature' ? this.id : ''   
      }

      this.authenticationService.getprouctdata(data).subscribe((res: any) => {

        this.length = res.total;
        this.search_result = res.data;

        this.pagehide = false;
        if (res.status) {
          this.pricehide = true;
          this.productdetail = res.data;
          this.subproduct = true;
          this.searchlist = false;
          this.searchresulthide = true;
        }
      })
    }
  }
  getsearchdata(value) {
   
    const data =
    {
      "limit": 100,
      "skip": 0,
      "searchText": value,
      "userId": localStorage.getItem('userid')
    }
    this.authenticationService.searchproduct(data).subscribe((res: any) => {
      this.pricehide = false;
      this.searchresulthide = false;
      this.length = res.total;
      this.search_result = res.data;
      this.searchlist = false;
      this.subproduct = false;
      if (this.search_result.length == 0) {
        this.router.navigate(['/notfound']);
      }

    });
  }


  getcategoryid(catid, subid, i, name) {
    this.router.navigate(['/categories'], { queryParams: { 'prod': name ,'checkList':'CheckProductCat'} })

    console.log("getcategoryid")
    this.subcatnumb = subid;
    this.index = i
    if (subid == 1) {
      this.searchlist = true;
      this.searchresulthide = true;
      this.subproduct = false;
      this.categoryid = catid;
      this.authenticationService.getcategoryTablist(catid).subscribe((res: any) => {
        this.pagehide = true;
        this.pricehide = false;
        this.bannerlist = res.data[1].value;
        this.subcategeorylist = res.data[2].value;
        this.listsubcategory = res.data[3].value;
        // console.log("catpro",res.data);
      })
    }
    else {
      // categories?id=5f9badc17415535ffcb80645&catid=5f9ba18d1638003ad420bf0c&name=subproduct&prod=Category
      this.router.navigate(['/categories'], { queryParams: { 'prod': 'Category','name':'subproduct','id':catid ,'catid':this.categoryid} })
      this.realid = catid;
      this.realcategory = this.categoryid;
      var minValue = JSON.stringify(this.value)
      const data =
      {
        "id": this.userid,
        "page": 1,
        "count": 10,
        "category": catid,
        "isRecommended": false,
        "isFeatured": false,
        "minPrice": minValue.toString(),
        "maxPrice": this.highValue.toString(),
        "discount": [],
        "categories": [this.realcategory],
        "subCategories": [catid]
      }
      console.log("prodtcegtidasd--")
      this.authenticationService.getprouctdata(data).subscribe((res: any) => {
        // console.log("productdata",res);

        this.length = res.total;
        this.search_result = res.data;
        this.pricehide = true;
        this.pagehide = false;
        if (res.total != 0) {
          if (res.status) {
            //   this.pricehide = true;
            this.productdetail = res.data;
            this.subproduct = true;
            this.searchlist = false;
            this.searchresulthide = true;
            this.route.queryParams.subscribe(params => { this.prod = params['prod'] });
          }
        } else {
          this.router.navigate(['/banner']);
        }
      })
    }

  }

  gotoproductDetails(id,type) {
    if(type == 'category')
    {
      this.router.navigate([],{queryParams:{'name':'moretoexplore','id':id}})
      const res = { 'id': localStorage.getItem('userid'), 'catid':id }
      setTimeout(() => {
        this.headerproductcategory(res)  
      }, 1000);
      
    }
    if(type == 'product')
    {
      this.router.navigate(['productdetail'], { queryParams: { 'id': id } });
     
    }
    if(type == 'subCategory')
    {
      this.router.navigate([],{queryParams:{'name':'searchsubprod','id':id}})
      const res = { 'id': id }
      setTimeout(() => {
        this.headerproductcategory(res)  
      }, 1000);
      
    }
    
  }



  changesearchresult(value) {
    if (value.length == 0) {
      this.router.navigate(['/home'])
    }
    else {
      this.getsearchdata(value);
    }
  }

  getbannnerdatalist(i, subcatl) 
  {
    this.subcatnumb = subcatl;
    this.bannerlist_id = i._id
    this.bannerlist_list = i.offer.list
    this.bannerlist_type = i.offer.type
    const data =
    {
      "count": "10",
      "page": "1",
      "id": this.bannerlist_id,
      "list": this.bannerlist_list,
      "type": this.bannerlist_type,
      "sortBy": this.checkstatus ? this.checkstatus : 1,
      "minPrice": this.value,
      "maxPrice": this.highValue,
      "brands": this.brandarr.length > 0 ? this.brandarr : '',
      "rating": this.radiocheck
    }

    this.authenticationService.getbannerprolist(data).subscribe((res) => {
      this.length = res.total;
      this.search_result = res.data;
      this.pagehide = false;
      if (res.status) {
        this.pricehide = true;
        this.productdetail = res.data;
        this.subproduct = true;
        this.searchlist = false;
        this.searchresulthide = true;
      }
    })
  }

  backtohome() {
    this.router.navigate(['/'])
  }
  // get feature product
  getFeatureProduct(res) {
    console.log("headerproductcategory12")
    this.realid = res.id;
    this.realcategory = res.catid;
    this.categoryid = res.catid;
    this.subcatnumb = 2;
    var minValue = JSON.stringify(this.value)
    const data =
    {
      "id": localStorage.getItem('userid'),
      "page": 1,
      "count": 10,
      "minPrice": minValue.toString(),
      "maxPrice": this.highValue.toString(),
      "brands": this.brandarr.length > 0 ? this.brandarr : '',
      "rating": this.radiocheck,
      "sortBy": this.checkstatus ? this.checkstatus : 1,
      "subCategory":res.catid
    }
    this.authenticationService.getprouctdata(data).subscribe((res: any) => {
      this.search_result = res.data;
      this.length = res.total;
      if (res.status) {
        this.productdetail = res.data;
        this.pricehide = true;
        this.subproduct = true;
        this.searchlist = false;
        this.searchresulthide = true;
      }
    })
  }
// -----------------------------------------  
  headerproductcategory(res) {
    console.log("headerproductcategory")
    this.realid = res.id;
    this.realcategory = res.catid;
    this.categoryid = res.catid;

    this.subcatnumb = 2;
    var minValue = JSON.stringify(this.value)
 
      const data =
      {
        "id": localStorage.getItem('userid'),
        "page": 1,
        "count": 10,
        "category": res.catid,
        "isRecommended": false,
        "isFeatured": false,
        "minPrice": minValue.toString(),
        "maxPrice": this.highValue.toString(),
        "brands": this.brandarr.length > 0 ? this.brandarr : '',
        "rating": this.radiocheck,
        "sortBy": this.checkstatus ? this.checkstatus : 1,
        "categories":this.prod == 'Feature' || this.name == 'moretoexplore' ? "" :  [this.categoryid],
        "subCategories":this.prod == 'Feature' || this.name == 'moretoexplore' ? "" : [this.realid],
        "subCategory":this.prod == 'Feature'  ? this.realid : ''
      }
      if(this.name == 'searchsubprod') data.categories = [];
    
    this.authenticationService.getprouctdata(data).subscribe((res: any) => {
      this.search_result = res.data;
      this.length = res.total;
      if (res.status) {
        this.productdetail = res.data;
        this.pricehide = true;
        this.subproduct = true;
        this.searchlist = false;
        this.searchresulthide = true;
      }
    })
  }

  headerProductCat(res)
  {
    console.log("headerprod",res)
    this.realid = res.id;
    this.realcategory = res.catid;
    this.categoryid = res.catid;
    this.subcatnumb = 2;
    var minValue = JSON.stringify(this.value)
    const data =
    {
      "id": localStorage.getItem('userid'),
      "page": 1,
      "count": 10,
      "category": res.id,
      "isRecommended": false,
      "isFeatured": false,
      "minPrice": minValue.toString(),
      "maxPrice": this.highValue.toString(),
      "categories":[res.catid],
      "subCategories":[res.id],
      "brands": this.brandarr.length > 0 ? this.brandarr : '',
      "rating": this.radiocheck,
      "sortBy": this.checkstatus ? this.checkstatus : 1,
    }
    this.authenticationService.getprouctdata(data).subscribe((res: any) => {
      this.search_result = res.data;
      this.length = res.total;
      if (res.status) {
        this.productdetail = res.data;
        this.pricehide = true;
        this.subproduct = true;
        this.searchlist = false;
        this.searchresulthide = true;
      }
    })
  }


  pageeventvalue(e) {
    if (this.subcatnumb == 1) {
      this.sliderEvent(e.pageIndex + 1, e.pageSize, 1)
    }
    if (this.subcatnumb == 2) {
      
      this.sliderEvent(e.pageIndex + 1, e.pageSize, 2)
      // this.realcategory = this.categoryid;
      // var minValue = JSON.stringify(this.value)
      // const data =
      // {
      //   "id": localStorage.getItem('userid'),
      //   "page": e.pageIndex + 1,
      //   "count": e.pageSize,
      //   "category": this.categoryid,
      //   "isRecommended": false,
      //   "isFeatured": false,
      //   "minPrice": minValue.toString(),
      //   "maxPrice": this.highValue.toString(),
      //   "brands": this.brandarr.length > 0 ? this.brandarr : '',
      //   "rating": this.radiocheck,
      //   "sortBy": this.checkstatus ? this.checkstatus : 1,
      //   "categories":this.prod == 'Feature' ? "" :  [this.categoryid],
      //   "subCategories":this.prod == 'Feature' ? "" : [this.realid],
      //   "subCategory":this.prod == 'Feature' ? this.realid : ''
      // }

      // this.authenticationService.getprouctdata(data).subscribe((res: any) => 
      // {
      //   this.length = res.total;
      //   this.search_result = res.data;

      //   this.pagehide = false;
      //   if (res.status) {
      //     this.pricehide = true;
      //     this.productdetail = res.data;
      //     this.subproduct = true;
      //     this.searchlist = false;
      //     this.searchresulthide = true;
      //   }
      // }
      // )
    }

    if (this.numcheck == 3) {
      this.sortSelected(1, e.pageIndex + 1, e.pageSize)
    }
  }

  rowmethod(e) {
    if (e == 1) {
      this.row = false;
      this.column = 'columncat'
    }
    else {
      this.row = true;
      this.column = 'rowcat'
    }
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }
  
  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }


  languageChange(lan)
  {
    this.translate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
   
    this.authenticationService.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }

  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }
}
