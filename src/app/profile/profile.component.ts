import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { TranslateService } from "@ngx-translate/core";
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { 
  AuthService 
} from 'ng4-social-login';
import { BehaviorSubject } from 'rxjs';
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO
} from "ngx-intl-tel-input";
import { browser } from 'protractor';
import { PageEvent } from '@angular/material';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [ AuthService]
})
export class ProfileComponent implements OnInit {
  hide = true;
  pass: any = {}
  displayCardInfo: boolean = false;
  public profiledata: any;
  pageEvent:PageEvent
  counteryiso:string="CountryISO.India";
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom
  ];
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  SentData:string;
  data : any;
  countryDataArray;
  viewIs;
  countryCodeData=CountryISO.India;

  public newPassword: any
  public oldPassword: any;
  public firstname: any;
  public lastname: any;
  public email: any;
  public phone: any;
  newemail: any;
  isEmailChanged: boolean = false;
  imageFile: any;
  profileForm: FormGroup;
  editaddressform: FormGroup;
  profilePic: any;
  googledata: any;
  show:boolean= false;
  address: any;
  file: any;
  setid:any;
  public imagePath;
  imgURL: any;
  imgaelink:any;
  countryList:any;
  loginType:any;
  submitted:boolean = false;
  addres_id:any;
  number:any;
  googlevalue:any;
  uppereimage:any;
  country:any;
  historyord:any;
  historypast:any;
  addaddressform:FormGroup;
  confirmErrorr: boolean;
  pageindexcount:any;
  length:any;
  lengthpast:any;
  isloggin:boolean = false;
  language: string;
  constructor(private router: Router, public authenticationService: AuthenticationService,
    private commonService: CommonService, private authService: AuthService,private translate:TranslateService,
    private _route: ActivatedRoute, private formBuilder: FormBuilder) {

      this.isloggin = JSON.parse(localStorage.getItem('sucess'));

      if(!localStorage.getItem('sucess'))
      {
        this.router.navigate(['/']);
      }
      this.googlevalue = JSON.parse(localStorage.getItem("googledata"));
    
      this.imgaelink=this.authenticationService.imageUrl;

    this.addaddressform  = this.formBuilder.group({
      apartment: [''],
      building: [''],
      state: "punjab",
      city: [''],
      postalCode: [''],
      name: [''],
      phone: [''],
      addressType: "Home",
      country:['',Validators.required],
      countryCode:[''],
      countryId:[''],
    });  

    this.profileForm = this.formBuilder.group({
      firstName: ['',Validators.required],
      lastName: ['',Validators.required],
      email: ['',Validators.email],
      phone: [''],
      profilePic: ['']
    });


    this.editaddressform = this.formBuilder.group({
      
      address1: ['',Validators.required],
      address2: ['',Validators.required],
      city: ['',Validators.required],
      postalCode: [''],
      name: ['',Validators.required],
      phone: ['',[Validators.required,Validators.minLength(5)]],
      addressType: "Home",
      _id:[''],
      country:['',Validators.required],
      countryCode:[''],
      countryId:[''],
      state:[''],

    });


    if(this.googlevalue != null)
    {
      this.profileForm.controls['firstName'].setValue(this.googlevalue.name.split(' ')[0]) ;
      this.profileForm.controls['lastName'].setValue(this.googlevalue.name.split(' ')[1]);
      this.profileForm.controls['email'].setValue(this.googlevalue.email);   
    }
    this.language = localStorage.getItem('lan')
  }

  ngOnInit() {

    this.authenticationService.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })

    this.editaddressform.reset();
    this.countryCodeData=CountryISO.India;


    this.authenticationService.getprofile().subscribe((profdata: any) => {
      this.profiledata = profdata.data;
        this.newemail = this.profiledata.email;
        console.log("profilepic",this.profiledata)
       
      // else{
      //   this.newemail = this.profileForm.value.email;
      // }
      this.profilePic = profdata.data.profilePic;
      this.uppereimage = this.profilePic
      this.profileForm.patchValue(this.profiledata)
     
    });

    this.authenticationService.orderHistory(1,10,'ongoing').subscribe((res:any)=>{
      console.log("histsdsdsory",res);
      this.historyord = res.data;
      this.length = res.total;
    });

    this.authenticationService.orderHistoryPast(1,10).subscribe((res:any)=>
    {
      this.historypast = res.data;
      this.lengthpast =  res.total;
    })

    this.authenticationService.Userdata.subscribe(userdata => {
    console.log("profilesocial", userdata)
     this.profiledata = userdata.data;
      this.profileForm.patchValue(this.profiledata)
    });

    this.authenticationService.getaddress().subscribe((data: any) => {
      this.address = data.data;
      
      console.log("address", this.address);
    })
    
    this.loginType = localStorage.getItem("provider");
 
  }

  public errorHandling = (control: string, error: string) => {
    return this.profileForm.controls[control].hasError(error);
  }
  getAllcountry()
  {
    const data=
    {
      'search':this.editaddressform.controls['country'].value
    }
    this.authenticationService.getallCountry(data).subscribe((res:any)=>
    {
      this.countryList = res.data;
    })
  }

  public errorHandlingAddress = (control: string, error: string) => {
    return this.editaddressform.controls[control].hasError(error);
  }

  logout() :void
  {
    this.authService.signOut();
    localStorage.removeItem('confirmauth');
    localStorage.removeItem('sucess');
    localStorage.removeItem('userid');
    localStorage.removeItem('googledata');
    localStorage.removeItem('provider');
    this.router.navigate(['/home']);
    localStorage.clear();
    this.addaddressform.reset();
    this.profileForm.reset();
    this.editaddressform.reset();
    this.authenticationService.sendprofilepic("");
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
 window.location.reload();
  }

  goToorderdetail(id,value) 
  {
    const data = {
      "id":id,
      "pagecount": 1,
      "type":value
    }
    this.authenticationService.tranferhistoryid(data);
    this.router.navigate(['/orderdetail']);
  }
                     //password chane
  onPasswordSubmit(f: NgForm) {
                  //  console.log("change", f);
    if (this.pass.newPassword != this.pass.cpassword) {
      this.confirmErrorr = true
               //    console.log(this.confirmErrorr, this.pass.newPassword, this.pass.cpassword)
    } else {
      this.confirmErrorr = false
      let form =
      {
        "oldPassword": this.pass.oldPassword,
        "newPassword": this.pass.newPassword,
        "confirmPassword": this.pass.cpassword

      }
            // console.log("f", form);
      this.authenticationService.changePassword(form).subscribe((res: any) => {

        if (res.success == true) {
          //   console.log(res)

          this.commonService.showToasterSuccess(this.language == 'en' ? "Password Updated Successfully" : "تم تحديث كلمة السر بنجاح");
          f.reset();
                  // this.router.navigate(["/riderequest"]);
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/profile']);

        }
        else {
          this.commonService.showToasterError(res.message);
        }
      });
    }
  }


  backtohome()
  {
    this.router.navigate(['/home']);
  }

  pageeventvalue(e,value)
  {
     console.log("pageeventvalue",e);
     if (e.pageIndex >= 0 || e.pageSize >= 5)
    {
      this.pageindexcount  = e.pageIndex + 1;
       if(value == 'ongoing')
       {
        this.authenticationService.orderHistory(this.pageindexcount,e.pageSize,value).subscribe((res:any)=>{
         console.log("history",res);
           this.historyord = res.data;
          this.length = res.total;
         });
      
       }
       if(value == "past")
       {
        this.authenticationService.orderHistoryPast(this.pageindexcount,e.pageSize).subscribe((res:any)=>
        {
          console.log("res.data",res.data);
          this.lengthpast = res.total;
          this.historypast = res.data;
        })
       }
    } 
  }

  editprofile()
   {
     this.submitted = true;
     this.show = false;
      
      if(this.newemail != this.profileForm.value.email)
      {
        if(this.profileForm.controls['email'].valid)
        {
          console.log("newemail",this.newemail)
          this.authenticationService.verifyemail(this.profileForm.value.email).subscribe((data: any) => {
                                       //  console.log("check", data);
               if(data.success)
               {
                  this.commonService.showToasterSuccess(this.language == 'en' ? "Please verify your email first in order to change it." : "يرجى التحقق من بريدك الإلكتروني أولاً من أجل تغييره.");
                  this.profileForm.controls['email'].setValue(this.profileForm.value.email);
                  this.newemail = this.profileForm.value.email;
                                     //  console.log("email",this.profileForm.value.email)
               }
            });
        }
        else{
          this.commonService.showToasterError(this.language == 'en' ? "Please Enter valid Email!" : "الرجاء إدخال بريد إلكتروني صحيح!")
        }
        }
        if(this.newemail === this.profileForm.value.email)
        {
          if(this.profileForm.valid)
          {
                      //  console.log(this.profileForm.value)
            this.authenticationService.changedata(this.profileForm.value).subscribe((res:any)=>
            {
              console.log("retus",res);
          
                  if(res.success)
                  {
                    
                    this.commonService.showToasterSuccess(res.message);
                    this.authenticationService.getprofile().subscribe((profdata: any) => {
                      this.profiledata = profdata.data;
                      this.profilePic = profdata.data.profilePic;
                      this.uppereimage = this.profilePic;
                      this.authenticationService.sendprofilepic(profdata.data);
                                //  console.log("data", profdata);
                      this.authenticationService.setaccountname(this.profiledata.firstName);
                      this.profileForm.patchValue(this.profiledata)
                      
                    });
                  }
              
            });
          }
          else
          { 
            console.log(this.profileForm.controls['phone'].value)
            if(this.profileForm.controls['phone'].invalid)this.commonService.showToasterError(this.language== 'en' ? this.profileForm.controls['phone'].value == null ? "Phone number is required":"Invalid phone number" : this.profileForm.controls['phone'].value == null ? "رقم الهاتف غير صحيح":"رقم الهاتف غير صحيح")
          }
        }
       
    

  //  console.log("vvv", this.profileForm.value);

  }

addaddress() {

  // console.log("pjone",this.addaddressform.value);
   if(this.addaddressform.valid)
   {
   
   }


  }
  deleteaddress(id)
  {
    this.setid = null;
    this.authenticationService.deleteaddress(id).subscribe((res:any)=>{
      if(res.success)
      {
        this.authenticationService.getaddress().subscribe((data: any) => {
          this.address = data.data;
          this.editaddressform.reset();
        })  
      }
    })
  }

  editaddressdata(id)
  {
    this.setid = id;
    this.authenticationService.getaddress().subscribe((data: any) => {
      this.address = data.data;
     // console.log("editeddat",this.address);
    })
    this.address = this.address.filter(r => r._id == id);
   // console.log("editaddress",this.address);
    this.editaddressform.patchValue(this.address[0]);
  
  }

  editaddressvalue()
  {
    this.submitted = true;
   // console.log("check",this.editaddressform.value)
   this.country = this.editaddressform.controls['country'].value
   this.countryList = this.countryList.filter(ele=>ele.name == this.country)
   if(this.countryList.length == 0)this.commonService.showToasterError(this.language == 'en' ? "Please enter correct country name!" : "الرجاء إدخال اسم الدولة الصحيح!")
   this.editaddressform.controls['countryCode'].setValue(this.countryList[0].code)
   this.editaddressform.controls['countryId'].setValue(this.countryList[0]._id)
   this.editaddressform.controls['country'].setValue(this.countryList[0].name)
    if(this.editaddressform.valid)
    {
   
      if(this.setid != null)
      {
        this.authenticationService.editaddress(this.editaddressform.value).subscribe((data:any)=>{
          if(data.success)
          {
         
            this.commonService.showToasterSuccess(this.language == 'en' ? "Address updated successfully!" : "تم تحديث العنوان بنجاح!");
           this.authenticationService.getaddress().subscribe((data: any) => {
             this.address = data.data;
             this.submitted = false;
             this.editaddressform.reset();
             this.setid = null;
           });
           }
        });
      }
     
       if(this.setid == null)
       {
         if(this.editaddressform.valid)
         {
        
          
          this.authenticationService.addaddress(this.editaddressform.value).subscribe((res:any)=>{
            //  console.log("addaddress",res);
             if(res.success)
             {
               this.commonService.showToasterSuccess( this.language == 'en' ? "Address Added successfully!" : "تمت إضافة العنوان بنجاح!")
              this.authenticationService.getaddress().subscribe((data: any) => {
                this.address = data.data;
                this.submitted = false;
                this.editaddressform.reset();
              }) 
             }
          });

         }
        
      
       }
    }
    else
    {
      console.log(this.editaddressform.controls['phone'].value)
   this.commonService.showToasterError(this.language == 'en' ? this.editaddressform.controls['phone'].value == null ? "Phone number is required" :"Invalid phone number" : this.editaddressform.controls['phone'].value == null ? "رقم الهاتف مطلوب" :"رقم الهاتف غير صحيح")
    }
  }

   onChange(event) {
     this.show = true;
    var files = event.srcElement.files;
    this.file = files[0];
   // console.log("fie",this.file)
    this.profileForm.controls['profilePic'].setValue(files[0]);
    if (event.target.files[0].type.indexOf("image/") == 0) {
    this.loadImage(this.file.type, files);
    }
    else{
      this.commonService.showToasterError(this.language == 'en' ? "Invalid Image!!!" : "صورة غير صالحة !!!");
    }
  }

  loadImage(mimeType, files) {
    console.log("--lasdjfjalsfdj", files[0]);
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
   
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL = reader.result;
      this.profilePic = this.imgURL;
         
    };
  }

  preventLeadingZero(event) {
    const input = event.target.value;
  
  
    if (input.length === 0 && event.which === 48) {
      event.preventDefault();
    }
  }
  checkphone(e)
  {
    if(e.charCode != 45)
    {
      return true;
    }
    else{
      return false;
    }
  }

  alphabate(event)
  {
   // alert(event.keyCode)
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 65 && charCode < 90) || (charCode > 97 && charCode < 122)) {
      return true;
    }
    return false;
  }
}
