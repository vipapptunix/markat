import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionverifyComponent } from './optionverify.component';

describe('OptionverifyComponent', () => {
  let component: OptionverifyComponent;
  let fixture: ComponentFixture<OptionverifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionverifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionverifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
