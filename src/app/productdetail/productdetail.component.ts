import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
declare const $: any;
@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.scss']
})
export class ProductdetailComponent implements OnInit {
  id: any;
  imgurl: any;

  productDetails: any;
  customRecommended: OwlOptions = {
    loop: false,
    mouseDrag: false,
    freeDrag:true,
    autoplay:false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    nav: true,
    margin: 25,
    autoWidth:false,

    navText: ['<<', '>>'],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 4
      }
    },
   
  }

  customRecommended2: OwlOptions = {
    loop: false,
    mouseDrag: false,
    freeDrag:true,
    autoplay:false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    nav: true,
    slideBy: 5,
    margin: 20,
    autoWidth:false,
    navText: ['<<', '>>'],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 5
      }
    },
   
  }

  language: any;
  specificationsDetail: any[] = [];
  imageshow: any;
  name: string;
  cartname: any;
  favproduct: any;
  GuestData:any;
  homeData: any;
  filterfav: any;
  description: any;
  Ratiingdata: any;
  isloggin: any;
  cartlist: any;
  highlights: any;
  proid: any;
  bestsellerData: any;
  element: any;
  guestArray:any[] = [];
  productImages: any[] = [];
  imageSelected: any;
  bestsellerDataVal: any[] = [];
  productdetails:any;
  shareObj = {
    // href: `https://appgrowthcompany.com:3083/api/app/redirectDeepLink?type=product&id=${this.id}`,
    hashtag: "#FACEBOOK-SHARE-HASGTAG"
  };
  namelan: any;
  specificationsDetaillan: any;
  descriptionlan: any;
  highlightslan: any;
  lang: any;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private commonService: CommonService,
    private translate:TranslateService,
    private _route: ActivatedRoute) {  
      // this._route.queryParams.subscribe(params => {this.language = params['lan']});
      this.authenticationService.languagechange.subscribe((res:any)=>
      {
        this.language = res;
      })
    }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.isloggin = localStorage.getItem("sucess");
    this.imgurl = this.authenticationService.imageUrl;
    this._route.queryParams.subscribe(params => { this.id = params['id'] });

    this.getproductdetails(this.id);
    this.getDashboard()
    this.getAllrating()
    
    this.authenticationService.$updatepoduct.subscribe((res)=>
    {
      // this._route.queryParams.subscribe(params => { this.id = params['relatedproduct'] });
      // if(this.id == 'related&product')
      // this.getproductdetails(res);
    })


  }


  getproductdetails(id) {
    
    this.proid = id;
    this.authenticationService.productDetail(id).subscribe((res: any) => {
      this.cartlist = res.data;
      this.cartname = res.data.name;
      this.productdetails = res.relatedProducts
      console.log("Product Details is", res);
      if (res.success) {
        this.productDetails = res.data;
        this.bestsellerDataVal = this.productDetails.isFavourite
        this.productImages = this.productDetails.images;
        if (this.productImages !== null && this.productImages !== undefined && this.productImages.length > 0) {
          this.imageSelected = this.productImages[0];
        }
          this.specificationsDetaillan = this.productDetails.specifications_ar;
          this.descriptionlan = this.productDetails.description_ar;
          this.namelan = this.productDetails.name_ar;
          this.highlightslan = this.productDetails.highlights_ar;


          this.specificationsDetail = this.productDetails.specifications;
          this.description = this.productDetails.description;
          this.name = this.productDetails.name;
          this.highlights = this.productDetails.highlights;
  


      }
    })
  }

  changeImage(id) {
    this.imageSelected = this.productImages.filter(ij => ij._id == id)[0];
    $(this).css('border', 'solid #000000')
  }

  goTocart(cart) {
console.log("$$$$$$$$$",cart)
    if (!this.isloggin) {
    
      const data =
          {
            "productId": this.cartlist._id,
            "amount": cart.price - (cart.price*cart.discount)/100,
            //"amount": (this.productDetails.price - ((this.productDetails.price*this.productDetails.discount)/100)),
            "quantity": 1
          }
        if(localStorage.getItem('cartcount'))
        {
          this.guestArray = JSON.parse(localStorage.getItem('cartcount'))
          if(this.guestArray)
          {
            var sum = 0;
            this.guestArray.forEach(element => {
              if(element.productId == cart._id)
              {
                element.quantity = element.quantity + 1;
                console.log("inside")
              }
          
            });
            if(this.guestArray.filter(ele => ele.productId == cart._id).length < 1)
            {
              this.guestArray.push(data)
            }
          }
          else
          {
              this.guestArray.push(data);
          }
          
          
        }else
        {
          this.guestArray.push(data)
        }
      this.router.navigate(['/cart']);

      console.log("GuestDAta",this.GuestData)
     
      localStorage.setItem('cartcount',JSON.stringify(this.guestArray))
    }
    else {
      const pstdata =
      {
        "data": [
          {
            "productId": this.cartlist._id,
            "amount": this.cartlist.price,
            //"amount": (this.productDetails.price - ((this.productDetails.price*this.productDetails.discount)/100)),
            "quantity": 1
          }
        ]
      }
      this.authenticationService.postcartlist(pstdata).subscribe((res: any) => {

        if (res.status) {
          this.router.navigate(['/cart']);
        }
      });
    }


  }

  gotoproductDetails(id){

    this.authenticationService.updateproduct(id)
    this.getproductdetails(id);
  
  }


  Addtobuy() {
    const pstdata =
    {
      "data": [
        {
          "productId": this.cartlist._id,
          "amount": this.cartlist.price,
          "quantity": 1
        }
      ]
    }
    if (this.isloggin) {
      this.authenticationService.postcartlist(pstdata).subscribe((res: any) => {

        if (res.status) {
          this.router.navigate(['/checkout']);
        }
      });
    }
  }

  goTocheckout() {
    this.router.navigate(['/checkout']);

  }
  goTologin() {
    this.router.navigate(['/login']);
  }
  goTosignup() {
    this.router.navigate(['/signup']);
  }


  addfavlist() {
    debugger
    if (this.isloggin != null) {
      this.authenticationService.postfavlist(this.proid).subscribe((res: any) => {
        if (res.success) {
          this.getDashboard()
        }
        console.log("postfav", res);
      });
    }

  }



  getDashboard() {
    
    var id = localStorage.getItem("userid")
    // console.log(id);
    if (localStorage.getItem('sucess') && localStorage.getItem('userid')) {
      this.authenticationService.getfavdashboard(id).subscribe((res: any) => {
        console.log("home data is", res.data);

        if (res.success) {

          this.homeData = res.data.output;

          this.bestsellerData = this.homeData.filter(ij => ij.label == "Best Seller")[0];
          console.log("this.best", this.bestsellerData)
          this.filterfav = this.bestsellerData.value.filter(i => i._id == this.proid)
          if (this.bestsellerData !== null && this.bestsellerData !== undefined) {
            this.bestsellerDataVal = this.filterfav[0].isFavourite;
            console.log("this.est", this.bestsellerDataVal)
          }

        }

      })
    }
    else {
      this.authenticationService.getDashboard().subscribe((res: any) => {


        if (res.success) {

          this.homeData = res.data.output;



          this.bestsellerData = this.homeData.filter(ij => ij.label == "Best Seller")[0];
          this.filterfav = this.bestsellerData.filter(i => i._id == this.proid)
          console.log("fgdgfd", this.filterfav);
          if (this.bestsellerData !== null && this.bestsellerData !== undefined) {
            this.bestsellerDataVal = this.bestsellerData.value;
            console.log("bestseller", this.bestsellerDataVal);
          }

        }

      })
    }
  }

  getAllrating() {
    const data =
    {
      'id': this.id,
      'page': 1,
      'count': 10
    }
    this.authenticationService.getAllRating(data).subscribe((res: any) => {
      this.Ratiingdata = res.data
      console.log("rating", res.data)
    })
  }

  moveto() {
    this.router.navigate(['/']);
  }
  //     magnify(event)
  // {
  //     this.element = document.getElementById("overlay");
  //     this.element.style.visibility = "visible";

  //     var x = event.clientX;     // Get the horizontal coordinate
  //     var y = event.clientY;     // Get the vertical coordinate

  //     this.element.style.top = y - 80;
  //     this.element.style.left = x - 80;
  // }
  // outimage()
  // {
  //     var element = document.getElementById("overlay");
  //     element.style.visibility = "hidden";
  // }

  // magnify(event) {
  //   var element = document.getElementById("overlay");
  //   element.style.display = "inline-block";
  //   var img = document.getElementById("imgZoom");
  //   var posX = event.offsetX ? (event.offsetX) : event.pageX - img.offsetLeft;
  //   var posY = event.offsetY ? (event.offsetY) : event.pageY - img.offsetTop;
  //   element.style.backgroundPosition=(-posX*2)+"px "+(-posY*4)+"px";

  // }

  // outimage() {
  //   var element = document.getElementById("overlay");
  //   element.style.display = "none";
  // }



  // $("#exzoom").exzoom({

  //   // thumbnail nav options
  //   "navWidth": 60,
  //   "navHeight": 60,
  //   "navItemNum": 5,
  //   "navItemMargin": 7,
  //   "navBorder": 1,

  //   // autoplay
  //   "autoPlay": true,

  //   // autoplay interval in milliseconds
  //   "autoPlayTimeout": 2000

  // });
  // magnify() {
  // 
  //   this.imageshow = true;
  //   var img, lens, result, cx, cy;
  //   img = document.getElementById('bigimage');
  //   result = document.getElementById('myresult');
  //   /*create lens:*/
  //   lens = document.createElement("DIV");
  //   lens.setAttribute("class", "img-zoom-lens");
  //   /*insert lens:*/
  //   img.parentElement.insertBefore(lens, img);
  //   /*calculate the ratio between result DIV and lens:*/
  //   cx = result.offsetWidth / lens.offsetWidth;
  //   cy = result.offsetHeight / lens.offsetHeight;
  //   /*set background properties for the result DIV:*/
  //   result.style.backgroundImage = "url('" + img.src + "')";
  //   result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
  //   /*execute a function when someone moves the cursor over the image, or the lens:*/
  //   lens.addEventListener("mousemove", moveLens);
  //   img.addEventListener("mousemove", moveLens);
  //   /*and also for touch screens:*/
  //   lens.addEventListener("mousemove", moveLens);
  //   img.addEventListener("mousemove", moveLens);
  //   function moveLens(e) {
  //     alert('')
  //     var pos, x, y;
  //     /*prevent any other actions that may occur when moving over the image:*/
  //     e.preventDefault();
  //     /*get the cursor's x and y positions:*/
  //     pos = getCursorPos(e);
  //     /*calculate the position of the lens:*/
  //     x = pos.x - (lens.offsetWidth / 2);
  //     y = pos.y - (lens.offsetHeight / 2);
  //     /*prevent the lens from being positioned outside the image:*/
  //     if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
  //     if (x < 0) {x = 0;}
  //     if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
  //     if (y < 0) {y = 0;}
  //     /*set the position of the lens:*/
  //     lens.style.left = x + "px";
  //     lens.style.top = y + "px";
  //     /*display what the lens "sees":*/
  //     result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
  //   }
  //   function getCursorPos(e) {

  //     var a, x = 0, y = 0;
  //     e = e || window.event;
  //     /*get the x and y positions of the image:*/
  //     a = img.getBoundingClientRect();
  //     /*calculate the cursor's x and y coordinates, relative to the image:*/
  //     x = e.pageX - a.left;
  //     y = e.pageY - a.top;
  //     /*consider any page scrolling:*/
  //     x = x - window.pageXOffset;
  //     y = y - window.pageYOffset;
  //     return {x : x, y : y};
  //   }
  // }

  // outimage()
  // {
  //   this.imageshow = false;
  // }


  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }
  languageChange(lan)
  {
    this.translate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
    if(this.lang == 'en') 
    if(this.lang == 'arb') 
    this.authenticationService.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }
  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }
}
