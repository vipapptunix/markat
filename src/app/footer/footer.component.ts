import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/auth.service';
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  footerdata:any; 
  subcatlist: any;
  name:string;
  arraylist:any[] = [];
  termsvalue:any;
  language: string;
  constructor(private authservice:AuthenticationService,private route:Router,private router: ActivatedRoute,private translate:TranslateService) 
  { 
    this.language = localStorage.getItem('lan')
    this.authservice.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })
  }

  ngOnInit() 
  {
    this.router.queryParams.subscribe(params => { this.name = params['name'] }); 
    this.authservice.getDashboard().subscribe((res:any)=>{
      this.footerdata = res.data.output;
    
     this.footerdata=this.footerdata.filter(ij=>ij.label=="Shop By Category")[0].value;
  
     });

    //  this.authservice.getbannersubcat().subscribe((res:any)=>
    //  {
    //    this.subcatlist = res.data
     
    //  })
     this.authservice.gettermsandpolicy().subscribe((res)=>
     {
      this.termsvalue = res.data;
      this.arraylist = ["Return Policy","Vendor Terms and Conditions","Terms of Use","Terms of Sales","Warranty Policy","Term and Condition"]
       this.termsvalue = this.termsvalue.filter(ele => ele.title != this.arraylist[0] && ele.title != this.arraylist[1] && ele.title != this.arraylist[2]  && ele.title != this.arraylist[3]  && ele.title != this.arraylist[4]  && ele.title != this.arraylist[5] )
     })
  }


  gotoprodcategory(catid,subid,name)
  {
  //  "categories?id=5f0408bf01e0501ddef617fd&catid=5eb9151da49e9d6d7a988869&name=subproduct"
  if(this.name == 'subproduct')this.authservice.getfooterProd()
  this.route.navigate(['/categories'],{queryParams: {'id':subid,'catid':catid,'name':'subproduct','prod':name}})
  }

  termsPage(i)
  {
    this.authservice.gettermsvalue(i);
    this.route.navigate(['/terms&cond'],{queryParams: {check:i}})
  }
  getredirect(e)
  {
   if(e == 'Face')
   {
     window.open('https://www.facebook.com/nunumegastore');
   }
   if(e == 'Link') 
   {
window.open('https://www.instagram.com/nunustores/?igshid=1il4saswoxftn')
   }
   if(e == 'Twitter')
   {
    window.open('https://twitter.com/nunumegastore')
   }
  }
}
