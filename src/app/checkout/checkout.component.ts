import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
declare var goSell: any;
export interface DialogData {
  animal: string;
  name: string;
}

import {
  SearchCountryField,
  TooltipLabel,
  CountryISO
} from "ngx-intl-tel-input";
import { NotificationService } from '../services/notification.service';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent {

  counteryiso:string="CountryISO.India";
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom
  ];
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  SentData:string;
  data : any;
  countryDataArray;
  viewIs;
  country:any;
  countryCodeData=CountryISO.India;
  submitted:boolean = false;
  imgUrl:any;
  public cartdata:any;
  address:any;
  taxCharge:any;
  addresszero:any;
  maxPurchase:boolean = false;
  animal: string;
  shippingCharge:any;
  name: string;
  productList:any;
  amount:any;
  checkstate:boolean = false;
  i:any;
  addressId:any;
  productId:any;
  setid:any;
  mode:any;
  close:any;
  checkdata:any;
  address11:any;
  paymentmode:any = 'COD';
  countcart:any;
  price_total:any;
  y:any;
  pricedata:any;
  addaddressform:FormGroup;
  countryList: any;
  prod_id: any;
  language: any;
  constructor(public dialog: MatDialog, private router: Router,private translate: TranslateService,public authservice:AuthenticationService,private commonService: CommonService, private formBuilder: FormBuilder,private notify:NotificationService) 
  {
    this.addaddressform  = this.formBuilder.group({
      address1: ['',Validators.required],
      address2  : ['',Validators.required],
      state: "punjab",
      city: ['',Validators.required],
      postalCode: [''],
      name: ['',Validators.required],
      phone: ['',Validators.required],
      country:['',Validators.required],
      countryCode:[''],
      countryId:[''],
      addressType: "Home",
      _id:['']
    }); 
    this.authservice.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })
   }

  openDialog(): void 
  {
    console.log(this.paymentmode)
  if(this.paymentmode == 'CC' )
  {
    const value ={
      "products":[
    this.productId  
    ],
    "deliveryAddress": this.addressId,
    "paymentMethod":this.paymentmode,
    "redirectUrl":'https://appgrowthcompany.com/mokabookweb/#/'
    }
    this.authservice.placedorder(value).subscribe((res:any)=>
    {
      if(res.status)
      {
        window.location.replace(res.data.payLink)
        var userid =localStorage.getItem('userid')
        // this.commonService.showToasterSuccess("Order placed sucessfully!")
        // this.notify.socket.on(`${userid}-notification`,(data)=>
        // {
        //   console.log("socket--->",data);
        //   var option = {
        //     'body': data.body,
        //     'silent': false,
        //   }
         // this.commonService.showToasterSuccess(data.body)
          //var notification = new Notification('Mokabook', option);
       // })

        // const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
        //   width: '250px',
        //   data: { name: this.name, animal: this.animal }
        // });
    
        // dialogRef.afterClosed().subscribe(result => {
        //   console.log('The dialog was closed');
        //   this.router.navigate(['/home']);
        //   this.animal = result;
        // });
 
      }
      else
      {
        this.commonService.showToasterError(this.language == 'en' ? "Please select the address!" : 'الرجاء تحديد العنوان!');;
      }
    }) 
  }
  if(this.paymentmode == 'COD' )
  {
    const value ={
      "products":[
    this.productId  
    ],
    "deliveryAddress": this.addressId,
    "paymentMethod":this.paymentmode,
    //"redirectUrl":'https://appgrowthcompany.com/mokabookweb/#/'
    }
    this.authservice.placedorder(value).subscribe((res:any)=>
    {
      if(res.status)
      {
        // window.location.replace(res.data.payLink)
         //var userid =localStorage.getItem('userid')
        this.commonService.showToasterSuccess(this.language ? "Order placed sucessfully!" : 'تم وضع الطلب بنجاح!')
      //   this.notify.socket.on(`${userid}-notification`,(data)=>
      //   {
      //     console.log("socket--->",data);
      //     var option = {
      //       'body': data.body,
      //       'silent': false,
      //     }
      //    this.commonService.showToasterSuccess(data.body)
      //     var notification = new Notification('Mokabook', option);
      //  })

        const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
          width: '250px',
          data: { name: this.name, animal: this.animal }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          this.router.navigate(['/home']);
          this.animal = result;
        });
 
      }
      else
      {
        this.commonService.showToasterError(this.language == 'en' ? "Please select the address!" : 'الرجاء تحديد العنوان!');
      }
    }) 
  }
  if(this.paymentmode == undefined)
  {
    this.commonService.showToasterError(this.language == 'en' ? 'Please select payment method' : 'الرجاء تحديد طريقة الدفع')
  }
  }


  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.imgUrl = this.authservice.imageUrl;
    this.authservice.getcartlist().subscribe((res:any)=>{
     this.cartdata = res.data;
    console.log("cartdatalist",this.cartdata);
    //this.amount = res.data[0].amount * res.data[0].quantity;
     this.productId = res.data[0].productId._id;
    // console.log("products",this.productId);

   })

   this.authservice.getaddress().subscribe((data: any) => {
    this.address = data.data;
    //this.addresszero = data.data;
    //this.addressId=data.data[0]._id;
   // console.log("address", this.addressId);
   // console.log("address11", this.addresszero);
  
  })


  this.authservice.getcartlist().subscribe((res:any)=>{
    this.getcartcount(res.data.length);
  })

  
  this.authservice.getshippingCharge('').subscribe((res:any)=>
  {
     this.taxCharge = res.data.tax;
    // console.log("------->",res.data.tax);
  })

  }
  public errorHandlingAddress = (control: string, error: string) => {
    return this.addaddressform.controls[control].hasError(error);
  }

  getAllcountry()
  {
    const data =
    {
      'search':this.addaddressform.controls['country'].value
    }
    this.authservice.getallCountry(data).subscribe((res:any)=>
    {
      this.countryList = res.data;
    })
  }
  goTohome()
  {
    this.router.navigate(['goTohome']);
  }

  radioclick(id,e)
  {
   this.authservice.getaddress().subscribe((data: any) => {
    this.addresszero  = data.data.filter(i=>i._id == id);
    this.addressId = this.addresszero[0]._id
    const val = 
    {
      "address":this.addressId
    }
    this.authservice.getshippingCharge(val).subscribe((res:any)=>
    {
      this.shippingCharge = this.price_total < res.data.shippingCharges.minPrice ? res.data.shippingCharges.belowPrice : this.price_total < res.data.shippingCharges.rangePrice ;
      console.log("checkbox-cehcked",this.shippingCharge); 
    })
    this.checkstate = true;
    console.log("address", data.data);
  })
  }

  productSelected(i)
  {
   console.log("productSelect",i);
  }

 //input search value
 onKeyInProduct(e)
 {
   this.productList = this.countryList.filter(name=>name.name == e)
   console.log(this.countryList)
   console.log(this.productList)
 }

  deleteaddress(id)
  {
    this.setid = null;
    this.authservice.deleteaddress(id).subscribe((res:any)=>{
      if(res.success)
      {
        this.authservice.getaddress().subscribe((data: any) => {
        this.address = data.data;
        this.y = null;
        this.addresszero = [];
        this.addressId = null;
        console.log("adresszero",this.addresszero);
        })  
      }
    })
  }


  gotohome()
  {
    this.router.navigate(['/home']);
  }
  removecart(id)
  {
    this.authservice.removecarddata(id).subscribe((data:any)=>{
      if(data.status)
      {
        if(this.checkstate)
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            this.cartdata = res.data;
            //this.addresszero = res.data;
            this.getcartcount(res.data.length);
            if(res.data.length == 0)
            {
              this.router.navigate(['/home']);
            }
          });
        }
        else
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            this.cartdata = res.data;
            this.getcartcount(res.data.length);
            if(res.data.length == 0)
            {
              this.router.navigate(['/home']);
            }
          });
        }
       
      }
    });
  }


  subquantity(cartdata)
  {
   // console.log("cart",cartdata);
    this.i = cartdata.quantity - 1;
    if(this.i != 0)
    {
      this.amount = cartdata.amount - cartdata.productId.price
      this.authservice.updatelist(cartdata,this.i,cartdata.productId.price).subscribe((res:any)=>
      {
        if(res.status)
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            console.log("updatesubquantity",res);
            this.cartdata = res.data; 
            this.getcartcount(res.data.length);
          });  
        }
      })
    }
  
  }

  addquantity(id)
  {
    debugger
    this.authservice.getcartlist().subscribe((res:any)=>
    {
      this.checkdata = res.data.filter(i=>i._id == id)[0];
     console.log("add-qantity check",this.checkdata) 
     this.prod_id = this.checkdata._id;
     this.i = this.checkdata.quantity + 1;
    if(this.checkdata.productId.purchaseQuantity >= this.i)
    {    
      this.amount = this.checkdata.amount + this.checkdata.productId.price
      console.log("amountt",this.amount);
      this.pricedata = this.checkdata.productId.price;
      if(res.status)
      {
         //  console.log("cart",this.checkdata);
           this.authservice.updatelist(this.checkdata,this.i,this.pricedata).subscribe((res:any)=>
           {
             console.log("updateacartlist",res);
             //this.amount = res.data.amount * res.data.quantity;
             if(res.status)
            {
               this.authservice.getcartlist().subscribe((res:any)=>{
                 this.cartdata = res.data; 
                 console.log("getcartlist",res.data);
                 this.getcartcount(res.data.length);
               });  
            }
           })
      }}
  
    });  
  }

  radioChange(e)
  {
    this.paymentmode = e.value;
  }

  getcartcount(count)
  {
   // console.log("q",count);
    this.countcart = 0;
    this.price_total = 0;
    for(var i=0;i<count;i++)
    {
        this.countcart =  this.cartdata[i].quantity + this.countcart;
        console.log("co",this.countcart)
        this.price_total = ((this.cartdata[i].productId.price * this.cartdata[i].quantity)-(this.cartdata[i].productId.price * this.cartdata[i].quantity * (this.cartdata[i].productId.discount)/100)) + this.price_total;
       console.log("ros",this.price_total);
    }
    this.authservice.getcartlength(this.countcart);

  }

  editaddressdata(id)
  {
    
    this.submitted = true;
    this.setid = id;
    this.authservice.getaddress().subscribe((data: any) => {
      this.address11 = data.data;
      //console.log("data",this.address);
    })
    this.address11 = this.address.filter(r => r._id == id);
    this.addaddressform.patchValue(this.address11[0]);
  
  }

  addaddress(value) {
    debugger
    this.submitted = true;
   this.country = this.addaddressform.controls['country'].value
   this.countryList = this.countryList.filter(ele=>ele.name == this.country)
   if(this.countryList.length == 0)this.commonService.showToasterError("Please enter correct country name!")
   this.addaddressform.controls['countryCode'].setValue(this.countryList[0].code)
   this.addaddressform.controls['countryId'].setValue(this.countryList[0]._id)
   this.addaddressform.controls['country'].setValue(this.countryList[0].name)
  // console.log("asd",this.setid);
   if(this.addaddressform.valid)
   {
  
     if(value == 2)
     {
       this.authservice.editaddress(this.addaddressform.value).subscribe((data:any)=>{
         if(data.success)
         {
           const val = 
          {
            "address":data.data._id
          }
          this.submitted = false;
           this.commonService.showToasterSuccess("Address updated successfully!");
            this.authservice.getshippingCharge(val).subscribe((res:any)=>
            {
               this.shippingCharge = res.data;
               console.log("------->",res.data);
            })
          this.authservice.getaddress().subscribe((data: any) => {
            this.address = data.data;
           // this.addresszero = this.address;
          //  this.dialog.close(DialogOverviewExampleDialog);
            this.addaddressform.reset();
          });
          }
       });
     }
    
      if(value == 1)
      {
        this.setid = null;
        if(this.addaddressform.valid)
        {
       
         this.authservice.addaddress(this.addaddressform.value).subscribe((res:any)=>{
           //  console.log("addaddress",res);
            if(res.success)
            {
              const data= 
              {
                "address":res.data._id
              }
              this.submitted = false;
              this.commonService.showToasterSuccess("Address Added successfully!")
              this.authservice.getshippingCharge(data).subscribe((res:any)=>
              {
                console.log(res);
              })
             this.authservice.getaddress().subscribe((data: any) => {
                this.address = data.data;
                this.addresszero = [];
                this.addressId = null;
              //   if(this.address.length == 1)
              //   {
              //  this.addresszero = this.address;
              //   }
               this.addaddressform.reset();

             }) 
            }
         });

        }
       
     
      }
   }
   else
   {
     if(this.addaddressform.controls['phone'].invalid)
     {
       console.log("address",this.addaddressform.value)
      this.commonService.showToasterError("Invalid phone number");
     }
    // if(this.addaddressform.controls['phone'].value === null)
    // {
    //   this.commonService.showToasterError("Invalid phone number");
    // }
   }
     }

     addnewaddress()
     {
      this.submitted = false;
       this.setid = null;
       this.addaddressform.reset();

     }

     checkphone(e)
     {
       if(e.charCode != 45)
       {
         return true;
       }
       else{
         return false;
       }
     }

     openPaymentPage()
     {
       goSell.config({
         containerID:"root",
         gateway:{
           publicKey:"pk_test_Vlk842B1EA7tDN5QbrfGjYzh",
           merchantId: null,
           language:"en",
           contactInfo:true,
           supportedCurrencies:"all",
           supportedPaymentMethods: "all",
           saveCardOption:false,
           customerCards: true,
           notifications:'standard',
           callback:(response) => {
               console.log('response', response);
           },
           onClose: () => {
               console.log("onClose Event");
           },
           backgroundImg: {
             url: 'imgURL',
             opacity: '0.5'
           },
           labels:{
               cardNumber:"Card Number",
               expirationDate:"MM/YY",
               cvv:"CVV",
               cardHolder:"Name on Card",
               actionButton:"Pay"
           },
           style: {
               base: {
                 color: '#535353',
                 lineHeight: '18px',
                 fontFamily: 'sans-serif',
                 fontSmoothing: 'antialiased',
                 fontSize: '16px',
                 '::placeholder': {
                   color: 'rgba(0, 0, 0, 0.26)',
                   fontSize:'15px'
                 }
               },
               invalid: {
                 color: 'red',
                 iconColor: '#fa755a '
               }
           }
         },
         customer:{
           id:null,
           first_name: "First Name",
           middle_name: "Middle Name",
           last_name: "Last Name",
           email: "demo@email.com",
           phone: {
               country_code: "965",
               number: "99999999"
           }
         },
         order:{
           amount: 100,
           currency:"KWD",
           items:[{
             id:1,
             name:'item1',
             description: 'item1 desc',
             quantity: '1',
             amount_per_unit:'00.000',
             discount: {
               type: 'P',
               value: '10%'
             },
             total_amount: '000.000'
           },
           {
             id:2,
             name:'item2',
             description: 'item2 desc',
             quantity: '2',
             amount_per_unit:'00.000',
             discount: {
               type: 'P',
               value: '10%'
             },
             total_amount: '000.000'
           },
           {
             id:3,
             name:'item3',
             description: 'item3 desc',
             quantity: '1',
             amount_per_unit:'00.000',
             discount: {
               type: 'P',
               value: '10%'
             },
             total_amount: '000.000'
           }],
           shipping:null,
           taxes: null
         },
        transaction:{
          mode: 'charge',
          charge:{
             saveCard: false,
             threeDSecure: true,
             description: "Test Description",
             statement_descriptor: "Sample",
             reference:{
               transaction: "txn_0001",
               order: "ord_0001"
             },
             metadata:{},
             receipt:{
               email: false,
               sms: true
             },
             redirect: "http://localhost/redirect.html",
             post: null,
           }
        }
       });
     }
}
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>, private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/home']);
  }

  selectaddres(id)
  {
    alert(id);
  }


  alphabate(event)
  {
   // alert(event.keyCode)
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 65 && charCode < 90) || (charCode > 97 && charCode < 122)) {
      return true;
    }
    return false;
  }

  //payment gateway//
 
}