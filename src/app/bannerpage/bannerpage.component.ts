import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-bannerpage',
  templateUrl: './bannerpage.component.html',
  styleUrls: ['./bannerpage.component.scss']
})
export class BannerpageComponent implements OnInit {
  isloggin: string;

  constructor(private router:Router) { }

  ngOnInit() {
    this.isloggin = localStorage.getItem("sucess");
  }

  movehome()
  {
    this.router.navigate(['/home']);
  }
  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }
}
