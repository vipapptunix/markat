import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from '../services/auth.service';

@Component({
  selector: 'app-categoriesdetail',
  templateUrl: './categoriesdetail.component.html',
  styleUrls: ['./categoriesdetail.component.scss']
})
export class CategoriesdetailComponent implements OnInit {

  public id:any;
  bannerlist: any;
  listsubcategory: any;
  subcategeorylist: any;
  imgurl: string;
  homeData: any;
  isloggin:any;
  searchvalue:string;
  bestsellerData: any;
  bestsellerDataVal: any;
  language: string;
  lang: any;
  constructor(private router: ActivatedRoute, private _route:Router,private translate:TranslateService,public authenticationService: AuthenticationService) { this.router.queryParams.subscribe(params => { this.id = params['id'] });
  this.imgurl = this.authenticationService.imageUrl;
  
  this.authenticationService.languagechange.subscribe((res:any)=>
  {
    this.language = res;
  })
}

  ngOnInit() 
  {

    this.language = localStorage.getItem('lan')
    this.isloggin = localStorage.getItem("sucess");
    this.imgurl=this.authenticationService.imageUrl;
    this.authenticationService.getcategoryTablist(this.id).subscribe((res: any) => {
      console.log("catva",res)
      this.bannerlist = res.data[1].value;
      this.subcategeorylist = res.data[2].value;
      this.listsubcategory = res.data[3].value;

    })
    this.router.queryParams.subscribe(params => {this.id = params['id']});
    this.getdashboard()
  
  }
  goTocategories() {
    this._route.navigate(['categories']);

  }
  goTosignup() {
    this._route.navigate(['/signup']);
  }

  goTologin() {
    this._route.navigate(['/login']);
  }





getdashboard()
{
  this.authenticationService.getDashboard().subscribe((res:any)=>{
     if(res.success){
   this.homeData=res.data.output;
   this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];
   if(this.bestsellerData!==null && this.bestsellerData!==undefined){
     this.bestsellerDataVal=this.bestsellerData.value;
     }
     }
   })
 }

 gotoproductDetails(id)
 {
  this._route.navigate(['productdetail'], { queryParams: { 'id': id }});
 }

  gotohome()
  {
    this._route.navigate(['/']);
  }

  checkdetailsimage(list)
  {
     this._route.navigate(['/categories'],{queryParams:{'id':list._id,'catid':this.id,'name':'subproduct','prod':'Category'}})
  
  }
    getbannerlist(data)
    {
      this.authenticationService.getbannerdata(data)
     
      this._route.navigate(['/categories'])          
    }
  
    Help()
    {
      this._route.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
    }

    languageChange(lan)
    {
      this.translate.use(lan)
      localStorage.setItem('lan',lan)
      // window.location.reload()
      this.lang = lan
     
      this.authenticationService.languageChange(lan)
     //this.router.navigate([],{queryParams:{lan}})
    }

    gotoWhatsapp()
    {
      window.open('https://api.whatsapp.com/send?phone=+918288932697')
    }
}
