import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { TranslateService } from "@ngx-translate/core";
import {AuthService,GoogleLoginProvider,SocialUser,FacebookLoginProvider} from 'ng4-social-login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  public user : any = SocialUser;

  rememberMe:boolean=false;
  email: any;
  password: any;
  userData: any;
  message;
  language: any;
  constructor(private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private _route: ActivatedRoute,
    private socialAuthService:AuthService,private translate:TranslateService) { 
      this.authenticationService.languagechange.subscribe((res:any)=>
      {
        this.language = res;
        console.log(res);
      })
    }

  ngOnInit() {

  this.language = localStorage.getItem('lan')
   
      
    if (sessionStorage.getItem("rememberMe")) {
      let rememberMe = JSON.parse(sessionStorage.getItem('rememberMe'));
      this.rememberMe = true
      this.email = rememberMe.email
      this.password = rememberMe.password
  }
  else{
    this.email = ''
    this.password = ''
  }
  }
  passwordVisibility($event){
    $event.preventDefault();
    this.hide = !this.hide;
  }
  goTosignup() {
    this.router.navigate(['signup']);

  }
  goToforgetpassword() {
    this.router.navigate(['/forgetpasswordemail']);

  }
  goTohome() {
    this.router.navigate(['/home']);

  }

  movetonext()
  {
    this.router.navigate(['/']);
  }

  Login(loginForm: NgForm) {  
    console.log(loginForm,"form")
    if((localStorage.getItem("fblogin"))){
      localStorage.removeItem("fblogin");
    }
    if(loginForm.value.rememberMe==true){
      var data = {
        email: loginForm.value.email,
        password: loginForm.value.password
      }
      sessionStorage.setItem("rememberMe",JSON.stringify(data));
     }
    else { 
      sessionStorage.setItem('rememberMe', '') 
    }
    const userId = 'user001';
    // this.messagingService.requestPermission(userId);
    // this.messagingService.receiveMessage();
    // this.message = this.messagingService.currentMessage;

  
      // loginForm.value.deviceId = this.messagingService.token;
      // loginForm.value.deviceType = "web";
      
    // this.authservices.login(loginForm.value);

    if (loginForm.value.email !== '' && loginForm.value.password !== '') {

      var data={
        "email":loginForm.value.email,
        "password":loginForm.value.password
      }

      this.authenticationService.login(data);

    }
    
}
googlelogin()
{
 this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => {
   this.user = userData;
   console.log("we",this.user)
   this.authenticationService.sociallogin(this.user);
   
   localStorage.setItem("provider",this.user.provider);
   this.authenticationService.getgooglefbdata(this.user);
  

 });
}

facebooklogin()
{
  this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
    this.user = userData;
  this.authenticationService.sociallogin(this.user);
  this.authenticationService.getgooglefbdata(this.user);
    console.log("fb",this.user);
    localStorage.setItem("provider",this.user.provider)
    })
}
}


