import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CanceldialogboxComponent } from '../canceldialogbox/canceldialogbox.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
//import { StarRatingComponent } from 'ng-starrating';
import {TranslateService} from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CommonService } from '../services/common.service';
@Component({
  selector: 'app-orderdetail',
  templateUrl: './orderdetail.component.html',
  styleUrls: ['./orderdetail.component.scss']
})
export class OrderdetailComponent implements OnInit {

  public history_id :any; 
  public historyord:any;
  historyordpast:any;
  status:any;
  Reciept:any;
  feedback:any;
  newratingvalue:any;
  ratingid:any;
  repreatedid:any;
  orderratingid:any;
  pagecount:any;
  type:any;
  public form: FormGroup;
  rating3:any;
  language: any;
  constructor(private router: Router,private commonService: CommonService,private translate:TranslateService,private authSrv:AuthenticationService,public dialog: MatDialog,private fb: FormBuilder)
   { 
    this.rating3 = 0;
    this.form = this.fb.group({
      rating: ['', Validators.required],
    })
   }

  ngOnInit() {
    this.authSrv.languagechange.subscribe((res:any)=>
 {
   this.language = res;
   
   console.log(res);
 })
 
    this.authSrv.historyid.subscribe((res:any)=>{
      // this.history_id = res;
      console.log("categorydataformmone",res);
      this.history_id = res.id;
      this.pagecount = res.pagecount;
      this.type = res.type
    })
    if(this.type  == 'ongoing')
    {
      this.authSrv.orderHistory(this.pagecount,100,'ongoing').subscribe((res:any)=>{
        this.historyord= res.data
       console.log("orderplace",res.data)
       this.historyord =this.historyord.filter(id => id._id == this.history_id);
       this.status = this.historyord[0].status
  
      });
    }
    if(this.type == 'past')
    {
      this.authSrv.orderHistoryPast(this.pagecount,100).subscribe((res:any)=>
      {
        this.historyordpast = res.data
        console.log("historyordpast",this.historyordpast);
        
        this.historyordpast = this.historyordpast.filter(id => id._id == this.history_id);
        this.status = this.historyordpast[0].status
        console.log("checkhitory",this.historyordpast);
      })
    }
    this.language = localStorage.getItem('lan')
  }
  goToorderdetail()
  {
    this.router.navigate(['/orderdetail']);
  }

  //for rating
  // onRate($event:{oldValue:number, newValue:number, starRating:StarRatingComponent}) {
  //   console.log(`Old Value:${$event.oldValue}, 
  //     New Value: ${$event.newValue}, 
  //     Checked Color: ${$event.starRating.checkedcolor}, 
  //     Unchecked Color: ${$event.starRating.uncheckedcolor}`);
  //     this.newratingvalue = $event.newValue;
  // }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(CanceldialogboxComponent, {
     
    });
  }
  backtohome(e)
  {
    if(e == 1)
    {
      this.router.navigate(['/home']);

    }
    if(e == 2)
    {
       this.router.navigate(['/profile'])
    }
  }

  orderrepeated(id)
  {
    this.repreatedid = id;
  }

  submitrepreated()
  {
    debugger;
    this.authSrv.getorderrepeated(this.repreatedid).subscribe((res:any)=>
    {
      
      if(res.status)
      {
        this.commonService.showToasterSuccess("order has been placed!")
          this.router.navigate(['/profile']);
      }
    })
  }

  checkorderid(id,orderid)
  {
    
    this.ratingid = id;
    this.orderratingid = orderid;
  }

  submitrating()
  {
      console.log(this.form.controls['rating'].value.length);
     const data = 
     {
       "productId":this.ratingid,
       "rating": this.form.controls['rating'].value,
       "review":this.feedback,
       "orderId": this.orderratingid
     }
     console.log("",data);
     if(this.feedback != null && this.form.controls['rating'].value.length != 0)
     {
      this.authSrv.getproductfeedback(data).subscribe((res:any)=>
      {
        this.commonService.showToasterSuccess("Thanks for your feedback!.");
        console.log("::::",res);
        this.router.navigate(['/profile']);
      })
     }if(this.feedback == null)
     {
        this.commonService.showToasterError("please enter feedback!")

     }
     if(this.form.controls['rating'].value.length == 0)
     {
       this.commonService.showToasterError("please select the rating.")
     }
     
  }

  getReciept(id)
  {
    // this.authSrv.printReciept(id).subscribe((res:any)=>
    // {
    //   this.Reciept = atob(res.data);
    //  console.log(this.Reciept)
    window.open(`https://appgrowthcompany.com:3083/api/app/receiptPDF?orderid=${id}`);
    //  win.document.body.innerHTML=this.Reciept;
    // })
  }

  contactUs()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }
}
