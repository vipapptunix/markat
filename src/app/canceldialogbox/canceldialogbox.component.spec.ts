import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanceldialogboxComponent } from './canceldialogbox.component';

describe('CanceldialogboxComponent', () => {
  let component: CanceldialogboxComponent;
  let fixture: ComponentFixture<CanceldialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanceldialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanceldialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
