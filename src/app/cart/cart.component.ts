import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from '../services/auth.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  imgUrl:any;
  checkdata:any;
  countcart = 0
  i:any;
  amount:any;
  isloggin:any;
  price_total:any;
  public cartdata:any;
  pricedata:any;
  Guestdata:any = [];
  Userdata:any = [];
  prod_id: any;
  idcheck: any;
  language: string;
  lang: any;
  constructor(private router: Router,private tanslate:TranslateService , public authservice:AuthenticationService) 
  {
    this.language = localStorage.getItem('lan')
    this.authservice.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    });
  }

  ngOnInit() 
  {
    this.isloggin = localStorage.getItem("sucess");
    if(this.isloggin != null)
    {
       document.getElementById('online').style.display = "block";
       document.getElementById('guest').style.display = "none";
    }
    else
    {
     document.getElementById('online').style.display = "none";
     document.getElementById('guest').style.display = "block";
    }
    if(this.isloggin == null)
    {
       this.idcheck = JSON.parse(localStorage.getItem('cartcount'))
      
       console.log(this.idcheck);
       this.idcheck.forEach(element => {
        this.authservice.productDetail(element.productId).subscribe((res: any) => {
          res.data.quantity = element.quantity
          this.Guestdata.push(res.data);

           })         
       });
     
        console.log("checkstatus",this.Guestdata)
        
    }
    this.imgUrl = this.authservice.imageUrl;
    if(this.isloggin != null)
    {
     this.authservice.getcartlist().subscribe((res:any)=>{
     this.cartdata = res.data;
     console.log("cart",this.cartdata);

     this.getcartcount(res.data.length);
   })
  }

  }
  
  ngAfterViewInit()
  {
    setTimeout(() => {
      this.guestcartcount(this.idcheck.length)
      
    }, 300);
  }

  goTocheckout()
  {
    this.router.navigate(['/checkout']);
  }

  gotohome()
  {
    this.router.navigate(['/home']);
  }

  removecart(id,status)
  {
    if(status == 'ON')
    {
      this.authservice.removecarddata(id).subscribe((data:any)=>{
        //    console.log("remove",data)
          if(data.status)
          {
            this.authservice.getcartlist().subscribe((res:any)=>{
              this.cartdata = res.data;
          //    console.log("data deleted",res.data)
              this.getcartcount(res.data.length);
            });
          }
        });
    }
    if(status == 'OFF')
    {
      if(this.idcheck.length > 1)
      {
        window.location.reload()
        this.idcheck =  this.idcheck.filter(ele => ele.productId != id)
        localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
        this.idcheck.forEach(element => {
         this.authservice.productDetail(element.productId).subscribe((res: any) => {
           res.data.quantity = element.quantity
           this.Userdata.push(res.data);
   
            })         
        });
          this.Guestdata = this.Userdata
      }
      if(this.idcheck.length == 1)
      {
        this.idcheck =  this.idcheck.filter(ele => ele.productId != id)
        localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
        this.Guestdata = [];
        this.router.navigate(['/'])
      }
  
    }

  }


  subquantity(cartdata)
  {
    debugger
  //  console.log("cart",cartdata);
    this.i = cartdata.quantity - 1;
    if(this.i != 0)
    {
      this.amount = cartdata.amount - cartdata.productId.price
      this.authservice.updatelist(cartdata,this.i,cartdata.productId.price).subscribe((res:any)=>
      {
        if(res.status)
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            this.cartdata = res.data; 
            this.getcartcount(res.data.length);
          });  
        }
      })
    }
  }

  addquantity(id)
  {
    this.authservice.getcartlist().subscribe((res:any)=>
    {
      this.checkdata = res.data.filter(i=>i._id == id)[0];
      this.prod_id = this.checkdata._id;
     console.log("id",this.checkdata) 
      this.i = this.checkdata.quantity + 1;
      if(this.checkdata.productId.purchaseQuantity >= this.i)
      {    
      this.amount = this.checkdata.amount + this.checkdata.productId.price
      this.pricedata = this.checkdata.productId.price;
      if(res.status)
      {
          
           this.authservice.updatelist(this.checkdata,this.i,this.pricedata).subscribe((res:any)=>
           {
             if(res.status)
             {
              this.authservice.getcartlist().subscribe((res:any)=>
              {
      //          console.log("cart",res);
                 this.cartdata = res.data; 
                 this.getcartcount(res.data.length);
              });  
             }
           })
      }
    }
    });
    
  }

  offlineAdd(id)
  {
    this.checkdata = this.Guestdata.filter(i=>i._id == id)[0];
    this.prod_id = this.checkdata._id;
   console.log("id",this.checkdata) 
    this.i = this.checkdata.quantity + 1;

    if(this.checkdata.purchaseQuantity >= this.i)
    {    
      this.checkdata.quantity = this.i;
    this.amount = this.checkdata.amount + this.checkdata.price
    this.pricedata = this.checkdata.price;
    this.idcheck.forEach(element => {
      if(element.productId == id)
      {
        element.quantity = this.i
      }
    });
    localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
    }
    this.guestcartcount(this.idcheck.length)
  }

  offlineSub(cartdata)
  {
    this.checkdata = this.Guestdata.filter(i=>i._id == cartdata._id)[0];
    this.prod_id = this.checkdata._id;
    this.i = this.checkdata.quantity - 1;
   console.log("id",this.checkdata) 
    if(this.i != 0)
    {
      this.checkdata.quantity = this.i; 
      this.amount = cartdata.amount - cartdata.price
      this.idcheck.forEach(element => {
        if(element.productId == cartdata._id)
        {
          element.quantity = this.i
        }
      });
      localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
    }
    this.guestcartcount(this.idcheck.length)
  }

  goTologin() {
    this.router.navigate(['/login']);
    }
    goTosignup() {
      this.router.navigate(['/signup']);
      }
  

  getcartcount(count)
  {
    //console.log(count);
    this.countcart = 0;
    this.price_total = 0;
    for(var i=0;i<count;i++)
    {
       this.countcart =  this.cartdata[i].quantity + this.countcart;
       //console.log("co",this.countcart)
       this.price_total = ((this.cartdata[i].productId.price * this.cartdata[i].quantity)-(this.cartdata[i].productId.price * this.cartdata[i].quantity * (this.cartdata[i].productId.discount)/100)) + this.price_total;
       //console.log("ros",this.price_total);
    }
    this.authservice.getcartlength(this.countcart);

  }

  guestcartcount(count)
  {
    this.countcart = 0;
    this.price_total = 0;
    for(var i =0;i<count;i++)
    {
      this.countcart = this.Guestdata[i].quantity + this.countcart;
      this.price_total = ((this.Guestdata[i].price * this.Guestdata[i].quantity) - (this.Guestdata[i].price * this.Guestdata[i].quantity * (this.Guestdata[i].discount)/100)) + this.price_total;
    }
  }

  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }

  languageChange(lan)
  {
    this.tanslate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
   
    this.authservice.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }

  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }
}
