import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {TranslateService} from '@ngx-translate/core';
import {SlidesOutputData, OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit ,AfterViewInit
{
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:true,
    autoWidth:false,
    freeDrag:true,
    autoplay:true,
    navSpeed: 70,
    navText: ['',''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }

  categoryoptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    autoWidth:false,
    freeDrag:false,
    autoplay:false,
    navSpeed: 70,
    navText: ['', ''],
    responsive: {
      0: {
        items: 3
      },
      400: {
        items: 8
      },
      740: {
        items: 8
      },
      940: {
        items: 8
      }
    },
    nav: false
  }

  customRecommended: OwlOptions = {
    loop: false,
    mouseDrag: false,
    freeDrag:true,
    autoplay:false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    nav: true,
    slideBy: 5,
    margin: 20,
    autoWidth:false,
    navText: ['<<', '>>'],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 4
      }
    },
   
  }

  customRecommended2: OwlOptions = {
    loop: false,
    mouseDrag: false,
    freeDrag:true,
    autoplay:false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    nav: true,
    slideBy: 5,
    margin: 20,
    autoWidth:false,
    navText: ['<<', '>>'],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 5
      }
    },
   
  }


  customdealday: OwlOptions = {
    loop: false,
    mouseDrag: false,
    freeDrag:true,
    autoplay:false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    nav: true,
    slideBy: 5,
    margin: 20,
    autoWidth:false,
    navText: ['<<', '>>'],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 5
      }
    },
   
  }

  customBrand: OwlOptions = {
    loop: false,
    mouseDrag: false,
    freeDrag:true,
    autoplay:false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    dotsEach:false,
    nav: true,
    slideBy: 1,
    margin: 20,
    autoWidth:false,
    navText: ['<<', '>>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
   
  }


homeData:any;
activeSlides: SlidesOutputData;
slidesStore: any[];
image:any;
bannerData:any;
categoryData:any;
featuredData:any;
bestsellerData:any;
offerData:any;
changecolor:boolean = false;
favicon:any;
moretoexplore:any[] = [];
recommendedData:any;
googleid:any;
checkstatus:any;
profiledata:any;
googledata:any;
bannerDataVal:any[]=[];
categoryDataVal:any[]=[];
featuredDataVal:any[]=[];
bestsellerDataVal:any[]=[];
bestseller:any[]=[];
offerDataVal:any[]=[];
recommendedDataVal:any[]=[];
slideConfig = { slidesToShow: 1, slidesToScroll: 1,autoplay: true,
autoplaySpeed: 3000,dots:true};
isloggin:any;
imgurl:any;
  mokabookPayStatus: any;
  language: any;
  lang: any;

  constructor( private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private route: ActivatedRoute, 
    private translate:TranslateService,
    private _route: ActivatedRoute,private spinner: NgxSpinnerService) 
    {
      this.isloggin = localStorage.getItem("sucess");
      // this.authenticationService.googlefbget.subscribe((res:any)=>
      // {
      //   this.googleid = res.id;
      //   localStorage.setItem("googleid",this.googleid);
      //   console.log("googledata",res);
      // });
      this.googledata =  localStorage.getItem("googledata");

      if(localStorage.getItem('sucess') && this.googleid == null)
      {
        
        this.authenticationService.getprofile().subscribe((profdata: any) => {
        
          this.profiledata = profdata.data._id;
          localStorage.setItem("userid",this.profiledata);
        //  console.log("this.profile",this.profiledata);
        });
      }
      this.authenticationService.languagechange.subscribe((res:any)=>
      {
        this.language = res
      })
      // this.route.queryParams.subscribe(params => {this.language = params['lan']});
  
      console.log("000000",this.language)
}

  ngOnInit(){

    this.language = localStorage.getItem('lan')
    console.log("1111",this.language)
    this.imgurl=this.authenticationService.imageUrl;
    this.favicon = 'assets/images/like.png';
    this.getHomedata();
    this.route.queryParams.subscribe(params => { this.checkstatus = params['gatewaySays'] 
    this.mokabookPayStatus = params['mokabookPayStatus']
  });
    console.log("-----------mokabookPayStatus------------>>>>>>>>>>>>>",this.checkstatus,this.mokabookPayStatus)

  }
  

  ngAfterViewInit()
  {
    this.spinner.show();
  }

  gotoproductDetails(id){

    this.router.navigate(['productdetail'], { queryParams: { 'id': id }});
    
  }

  getHomedata()
  {
    var id = localStorage.getItem("userid")
   
  if(localStorage.getItem('sucess') && localStorage.getItem('userid') && this.googleid == null)
    {
//   this.authenticationService.getfavdashboard(id).subscribe((res:any)=>{
 
//   if(res.success){

// this.homeData=res.data.output;
// this.bannerData=this.homeData.filter(ij=>ij.label=="Banner")[0];
// //console.log("banner",this.bannerData);

// if(this.bannerData!==null && this.bannerData!==undefined){
// this.bannerDataVal=this.bannerData.value;
// //console.log("this.bannerdataval",this.bannerDataVal);

// }
// this.categoryData=this.homeData.filter(ij=>ij.label=="Shop By Category")[0];

// if(this.categoryData!==null && this.categoryData!==undefined){
//   this.categoryDataVal=this.categoryData.value;
  
//   }
// this.featuredData=this.homeData.filter(ij=>ij.label=="Featured")[0];

// if(this.featuredData!==null && this.featuredData!==undefined){
//   this.featuredDataVal=this.featuredData.value;
//  // console.log("feature",this.featuredDataVal);
//   }


// this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];

// if(this.bestsellerData!==null && this.bestsellerData!==undefined){
//   this.bestsellerDataVal=this.bestsellerData.value;

//   }

// this.offerData=this.homeData.filter(ij=>ij.label=="Offers")[0];

// if(this.offerData!==null && this.offerData!==undefined){
//   this.offerDataVal=this.offerData.value;
  
//   }

// this.recommendedData=this.homeData.filter(ij=>ij.label=="Recommended")[0];
// if(this.recommendedData!==null && this.recommendedData!==undefined){
//   this.recommendedDataVal=this.recommendedData.value;
//         this.image =   this.recommendedDataVal[0].images[0].image;
//         //console.log(this.image)
//   }

//   }

// })
  }
else
{
  // this.authenticationService.getDashboard().subscribe((res:any)=>{
  //  // console.log("home data is", res.data );
 
  //   if(res.success){
  
  // this.homeData=res.data.output;
  // this.bannerData=this.homeData.filter(ij=>ij.label=="Banner")[0];
  // //console.log("banner",this.bannerData);
  
  // if(this.bannerData!==null && this.bannerData!==undefined){
  // this.bannerDataVal=this.bannerData.value;
  
  // }
  // this.categoryData=this.homeData.filter(ij=>ij.label=="Shop By Category")[0];
  
  // if(this.categoryData!==null && this.categoryData!==undefined){
  //   this.categoryDataVal=this.categoryData.value;
    
  //   }
  // this.featuredData=this.homeData.filter(ij=>ij.label=="Featured")[0];
  
  // if(this.featuredData!==null && this.featuredData!==undefined){
  //   this.featuredDataVal=this.featuredData.value;
  //  // console.log("feature",this.featuredDataVal);
  //   }
  
  
  // this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];
  
  // if(this.bestsellerData!==null && this.bestsellerData!==undefined){
  //   this.bestsellerDataVal=this.bestsellerData.value;
  //   }
  
  // this.offerData=this.homeData.filter(ij=>ij.label=="Offers")[0];
  
  // if(this.offerData!==null && this.offerData!==undefined){
  //   this.offerDataVal=this.offerData.value;
    
  //   }
  
  // this.recommendedData=this.homeData.filter(ij=>ij.label=="Recommended")[0];
  // if(this.recommendedData!==null && this.recommendedData!==undefined){
  //   this.recommendedDataVal=this.recommendedData.value;
  //         this.image =   this.recommendedDataVal[0].images[0].image;
  //        // console.log(this.image)
  //   }
  
  //   }
  
  // })
}
  }
  goTocategoriesdetail(id) {
    this.router.navigate(['/categoriesdetail'], { queryParams: { 'id': id }});

  }
  goTocategories(value,name,data) {
    console.log("check",data);
    this.router.navigate(['/categories'],{queryParams: {'prod':name,'name':'moretoexplore','id':value}});
    // this.router.navigate(['/categories']);
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

  goTocelebrityindex() {
    this.router.navigate(['/celebrityindex']);
  }
  goTosellerindex() {
    this.router.navigate(['/sellerindex']);
  }
  goTobrandindex() {
    this.router.navigate(['/brandindex']);
  }
  goTocelebrityprofile() {
    this.router.navigate(['/celebrityprofile']);
  }
  goTosellerprofile() {
    this.router.navigate(['/sellerprofile']);
  }

  addfavlist(id)
  {
   if(this.isloggin != null)
   {
    this.authenticationService.postfavlist(id).subscribe((res:any)=>
    {
      if(res.success)
      {
        this.changecolor = true;
        this.getfavlistdata()
      }
     // console.log("postfav",res);
    });
   }

  }


  getfavlistdata()
  {
  //   var id = localStorage.getItem("userid")
  //   this.authenticationService.getfavdashboard(id).subscribe((res:any)=>{
  //     if(res.success){
  //       this.homeData=res.data.output;
  //       this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];

  //       if(this.bestsellerData!==null && this.bestsellerData!==undefined){
  //         this.bestsellerDataVal=this.bestsellerData.value;
  //         }
  //         this.recommendedData=this.homeData.filter(ij=>ij.label=="Recommended")[0];
  //          if(this.recommendedData!==null && this.recommendedData!==undefined){
  //             this.recommendedDataVal=this.recommendedData.value;
  //             this.image =   this.recommendedDataVal[0].images[0].image;
  //         //    console.log(this.image)
  // }
  //     }
  //   });
  }

  bannerpage(data)
  {

    data.type="bannerlist"
    //data.push(obj);
    this.authenticationService.getbannerdata(data);
  
      this.router.navigate(['/categories'],{ queryParams: { 'prod': 'bannerlist' , 'id':data._id }})
  
  }

  featurePage(data)
  {
    data.type = 'Feature'
    this.authenticationService.getbannerdata(data);
    this.router.navigate(['/categories'],{ queryParams: { 'prod': 'Feature' , 'id':data._id }})

  }

  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }

  languageChange(lan)
  {
    this.translate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
    if(this.lang == 'en') 
    if(this.lang == 'arb') 
    this.authenticationService.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }
  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }
}
