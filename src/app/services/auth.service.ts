import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from './common.service';
// import { VerificationModalAuthComponent } from '../verification-modal-auth/verification-modal-auth.component';
// import { PhoneConfirmComponent } from '../phone-confirm/phone-confirm.component';
// import { VehicleModalComponent } from '../vehicle-modal/vehicle-modal.component';
// import { VehicleUpdateModalComponent } from '../vehicle-update-modal/vehicle-update-modal.component';
// import { VerificationModalAuthComponent } from '../auth/verification-modal-auth/verification-modal-auth.component';
// import { OutletModalComponent } from '../user/outlet-modal/outlet-modal.component';


@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  // baseUrl: any = environment.user;
  // baseUrl: string = "https://appgrowthcompany.com:3000/v1/user/";
  baseUrl: string = "https://appgrowthcompany.com:9079/api/app/";
  // http://192.168.1.98:3000/
  // imageUrl:string="https://appgrowthcompany.com:4003";
  imageUrl:string="https://appgrowthcompany.com:9079/";
  // baseUrlFood:string="https://appgrowthcompany.com:3000/v1/web/";
  // imageUrlFood:string="https://appgrowthcompany.com:3000";
  countryCode: any;
  authorization:any;
  // adminUrl:string="https://appgrowthcompany.com:3000/v1/admin/food/";
  public Userdata = new BehaviorSubject<any>([]);
  private loggedIn = new BehaviorSubject<boolean>(false);
  public hist_id = new BehaviorSubject<any>('');
  public homeprofile = new BehaviorSubject<any>('');
  public homeprofname = this.homeprofile.asObservable();
  public historyid = this.hist_id.asObservable();
  public googlefbdata = new BehaviorSubject<any>([]);
  public googlefbget = this.googlefbdata.asObservable();
  public search_value = new BehaviorSubject<any>([]);
  public $searchvalue = this.search_value.asObservable(); 
  public cartlen = new BehaviorSubject<any>('');
  public cart_length = this.cartlen.asObservable(); 
  public checkproduct = new  BehaviorSubject<any>([]);
  public $checkproductid = this.checkproduct.asObservable();
  public termscond = new BehaviorSubject<any>('');
  public languagechange = new BehaviorSubject<any>('');

  public $termscond = this.termscond.asObservable();

  public propic_header = new BehaviorSubject<any>([]);
  public $profilepic_check = this.propic_header.asObservable();

  public footer = new BehaviorSubject<any>('');
  public $footer = this.footer.asObservable();

  public updateprod = new BehaviorSubject<any>('');
  public $updatepoduct = this.updateprod.asObservable();
  guestprod:any[] = [];
  public bannerdata =  new BehaviorSubject<any>([]);
  public $bannerlist = this.bannerdata.asObservable();
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private router: Router,
    private commonService: CommonService,
    private _route: ActivatedRoute
  )
   {
    this.getCountryCode();
    this.authorization =localStorage.getItem('confirmauth');

  }
  getCountryCode() {
    this.http.get("assets/json/countryCode.json").subscribe(data => {
      this.countryCode = data;
    })
  }
  categoryProducts(data){
    return this.http.get<any>(this.baseUrl + `categoryProducts?category=${data.cat}&userId=${data.userid}`) ;
  }

  subCategoryProducts(data){
    
    return this.http.get<any>(this.baseUrl +'subCategoryProducts?subCategory='+data) ;
  }

  completeRegistration(obj) {
    return this.http.post<any>(`${this.baseUrl}signup`, obj);
  }

  sendVerificationLink(data){
   
    return this.http.post<any>(`${this.baseUrl}sendVerificationLink`,data);
  }

  resendVerification(data){
    return this.http.post<any>(`${this.baseUrl}resendVerification`,data);
  }


  forgetpassword(data){
    return this.http.post<any>(`${this.baseUrl}forgotPassword`,data);
  }


  languageChange(value)
  {
    this.languagechange.next(value);
  }

  cancelorder(dat)
  {
 
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization':this.authorization })
   }
    return this.http.post<any>(`${this.baseUrl}cancelOrder`,dat,httpOptions);
  }

  getfooterProd()
  {
    this.footer.next('footer');
  }

  productDetail(id){
    return this.http.get<any>(this.baseUrl +'productDetail?productId='+id) ;
  }

  removecarddata(id){
  
    // const _id = new HttpParams()
    //    _id.append('id',id)
    const authorization =localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization':authorization })
   }
    return this.http.put<any>(`${this.baseUrl}removeCart?id=${id}`,{},httpOptions);
   // return this.http.put<any>(this.baseUrl +'removeCart?id='+id,httpOptions) ;
  }
////get
  favoriteproduct()
  {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({'authorization':authorization})
    }

    return this.http.get<any>(`${this.baseUrl}getFavorites`,httpOptions);
  }
////post
postfavlist(id)
{
  const _id = new HttpParams().set('productId',id);
  const authorization = localStorage.getItem('confirmauth');
  const httpOptions = {
    headers: new HttpHeaders({'authorization':authorization})
  }

  return this.http.post<any>(`${this.baseUrl}favorites`,_id,httpOptions)
}
  
getbannersubcat()
{
  //return this.http.get<any>(`${this.imageUrl}api/admin/categories`)
}

  getprofile()
{
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}
  return this.http.get<any>(`${this.baseUrl}getProfile`,httpOptions);
}

changedata(value)
{
  
  const formdata = new FormData();
  formdata.append('profilePic',value.profilePic)
  formdata.append('phone',value.phone.number)
  formdata.append('countryCode',value.dialCode)
  formdata.append('firstName',value.firstName)
  formdata.append('lastName',value.lastName)
  formdata.append('email',value.email)


  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
  }
 
  return this.http.put<any>(`${this.baseUrl}updateProfile`,formdata,httpOptions);
}

updateproduct(id)
{
  this.updateprod.next(id)
}

verifyemail(email)
{
  const data = {
    "email": email
  }
  return this.http.post<any>(`${this.baseUrl}sendVerificationLink`,data);
     
}

reSendotp(data)
{
  return this.http.post<any>(`${this.baseUrl}resendVerification`,data);
}

tranferhistoryid(id)
{
  this.hist_id.next(id);
}

setaccountname(firstname)
{
this.homeprofile.next(firstname);
}

getgooglefbdata(infodata)
{
  this.googlefbdata.next(infodata);
}

searchdata(text)
{
  this.search_value.next(text);
}

getcartlength(lenth)
{
  this.cartlen.next(lenth);
}
 sendprofilepic(propic)
 {
   this.propic_header.next(propic);
 }

checkproductId(data)
{
  this.checkproduct.next(data);
}

verifyPhone(data)
{
  return this.http.post(`${this.baseUrl}verifyPhone`,data)
}

gettermsvalue(id)
{
  this.termscond.next(id);
}
 getbannerdata(data)
 {
   this.bannerdata.next(data);
 }

  login(form) {
    const baseUrl = this.baseUrl;
    console.log("form",form)
    const httpOptions = {
      headers: new HttpHeaders({ 'lang': 'en', 'country': 'india' })
    }
    if (form.email !== '' && form.password !== '') {
      this.loggedIn.next(true);
      console.log("dayta")
      this.http.post(baseUrl + `signin`, form)
        .subscribe(
          (data: any) => {
            console.log("inside login data",data);
           
            if (data.success== true) {
              localStorage.setItem("confirmauth", data.data.accessToken);
              localStorage.setItem("sucess",data.success);
              sessionStorage.setItem('x-auth',"1")
                this.guestprod = JSON.parse(localStorage.getItem('cartcount'))
                if(this.guestprod)
                {
                  this.guestprod.forEach(element => {
                    element.amount = element.amount * element.quantity
                  });
                  const datacheck = {
                    "data": this.guestprod
                  }
                  console.log("------------>",datacheck)
                  this.postcartlist(datacheck).subscribe((res:any)=>
                  {
                    console.log("addtocart",res);
                  })
                
                }
                setTimeout(() => {
                  this.router.navigate(['/']);
                }, 1000);
            } else {
              this.commonService.showToasterError(data.message);
            }

          } 
        )
    }
  }


  getprouctdata(value)
  {
    return this.http.post<any>(`${this.baseUrl}getProducts`,value);
  }

//terms&condition
gettermsandpolicy()
{
  return this.http.get<any>('https://appgrowthcompany.com:3083/api/admin/getAllStaticPages')
}

//getReciept
printReciept(id)
{
  return this.http.get<any>(`${this.baseUrl}receiptPDF?orderid=${id}`)
}

contactUs(data)
{
  return this.http.post(`${this.baseUrl}contactUs`,data)
}

//getcart
getcartlist(){
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}
return this.http.get<any>(`${this.baseUrl}getCartList`,httpOptions)
}

getallCountry(data)
{
  return this.http.get<any>(`${this.baseUrl}getCountries?page=1&count=6&search=${data.search}`)
}

//postcart
postcartlist(list)
{
  debugger
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}
return this.http.post<any>(`${this.baseUrl}addToCart`,list,httpOptions) 
}

//getAllbrand
getallbrand(data)
{
  return this.http.post<any>(`${this.baseUrl}getBrands`,data)
}

//updatecart
updatelist(value,i,j)
{
  console.log("auh",value)
  const payload = new HttpParams()
  .set('id', value._id)
  .set('productId', value.productId._id)
  .set('amount', j)
  .set('quantity', i)

  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}
return this.http.put<any>(`${this.baseUrl}updateCart`,payload,httpOptions)
}


getfavdashboard(id)
{
   var pageid:number = 1;
    const data =
    {
     "page":pageid ,
     "userId":id
    }
    return this.http.post<any>(`${this.baseUrl}dashboard`,data)
}

  getDashboard(){
    var pageid:number=1;
    const data =
    {
      "page":pageid
    }
     return this.http.post<any>(this.baseUrl +'dashboard',data) ;
   }


    changePassword(data){
     const authorization =localStorage.getItem('confirmauth');
    const httpOptions = {
     headers: new HttpHeaders({ 'authorization':authorization })
  }
  return this.http.post<any>(`${this.baseUrl}changePassword`, data,httpOptions);
  }

sociallogin(socialdata)
{
  const data = {
    "firstName": socialdata.name.split(" ")[0],
    "lastName": socialdata.name.split(" ")[1],
    "provider": socialdata.provider,
    "providerId": socialdata.id,
    "deviceType": "Web application",
    "pushToken": socialdata.token,
    "email": socialdata.email
}
  console.log("socialdata",data);
   this.http.post<any>(`${this.baseUrl}socialLogin`,data).subscribe((res:any) => {
    //console.log("sucess",res)
    console.log("sucessGoogle",res.data.accessToken)
    //localStorage.setItem("confirmauth",res.data.accessToken)
    localStorage.setItem("sucess",res.success);
    this.Userdata.next(res);
    console.log("sucess",this.Userdata)
    
     if(res.success)
     {
      localStorage.setItem("confirmauth",res.data.accessToken)
       this.router.navigate(['/']);
     }
   });

   //return data;
}

getbannerproduct(id,list)
{
  const data =
  {
    "count": "10",
    "page": "1",
    "id": id,
    "list": list,
    "type": "category",
    "sortBy": 1,
    "minPrice": 0,
    "maxPrice": 5000
  }
  return this.http.post<any>(`${this.baseUrl}getBannerProducts`,data);
}

getbannerprolist(data)
{
  return this.http.post<any>(`${this.baseUrl}getBannerProducts`,data);
}

getaddress()
{
  
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}

return this.http.get(`${this.baseUrl}address`,httpOptions);
}

addaddress(data)
{
  console.log("datat",data)
const payload = new HttpParams()
.set('address1', data.address1)
.set('address2', data.address2)
.set('state', 'punjab')
.set('city',data.city)
.set('postalCode',data.postalCode)
.set('name',data.name)
.set('phone', String(data.phone.number))
.set('addressType','Home')
.set('country',data.country)
.set('countryId',data.countryId)
.set('countryCode',data.countryCode)
 
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}

return this.http.post<any>(`${this.baseUrl}address`,payload,httpOptions)
}

deleteaddress(id)
{
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}
  return this.http.delete<any>(this.baseUrl +'address?id='+id,httpOptions)
}

editaddress(value)
{
  const payload = new HttpParams()
.set('address1', value.address1)
.set('address2', value.address2)
.set('state', value.state)
.set('city',value.city)
.set('postalCode',value.postalCode)
.set('name',value.name)
.set('phone', value.phone.number)
.set('addressType',value.addressType)
.set('country',value.country)
.set('countryCode',value.countryCode)
.set('countryId',value.countryId)

.set('id',value._id)
  const authorization =localStorage.getItem('confirmauth');
  const httpOptions = {
   headers: new HttpHeaders({ 'authorization':authorization })
}
return this.http.put<any>(`${this.baseUrl}address`,payload,httpOptions)
}

orderHistory(page,count,value)
{
  const data  = 
    {
      "page": page == 'undefined' ? 1 : page,
      "count": count,
      "type": value
    }
    console.log("authersiveccount",data);
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({'authorization': authorization})
    }
  return this.http.post<any>(`${this.baseUrl}orderHistory`,data,httpOptions)
  }

  orderHistoryPast(page,count)
  {
    const data  = 
    {
      "page": page,
      "count": count,
      "type":"past"
    }
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({'authorization': authorization})
    }
  return this.http.post<any>(`${this.baseUrl}orderHistory`,data,httpOptions)
  }

  placedorder(data)
  {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({'authorization': authorization})
    }
   return this.http.post<any>(`${this.baseUrl}placeOrder`,data,httpOptions);
  }

searchproduct(data)
{
  return this.http.post<any>(`${this.baseUrl}search_product`,data);
}

getcategoryTablist(id)
{
  const payload = new HttpParams()
  .set('category', id)
  
  return this.http.post<any>(`${this.baseUrl}getCategoryTab`,payload);
}

getAllRating(data)
{
  return this.http.get<any>(`${this.baseUrl}reviews?page=${data.page}&count=${data.count}&productId=${data.id}`)
}

getorderrepeated(id)
{
  const authorization = localStorage.getItem('confirmauth');
  const httpOptions = {
    headers: new HttpHeaders({'authorization': authorization})
  }
  const payload = new HttpParams()
  .set('orderId',id)
  return this.http.post<any>(`${this.baseUrl}repeatOrder`,payload,httpOptions)
}

getshippingCharge(data)
{
  return this.http.post(`${this.baseUrl}getCharges`,data)
}

getproductfeedback(data)
{
  const authorization = localStorage.getItem('confirmauth');
  const httpOptions = {
    headers: new HttpHeaders({'authorization': authorization})
  }
  const payload = new HttpParams()
  .set('productId',data.productId)
  .set('rating',data.rating)
  .set('review',data.review)
  .set('orderId',data.orderId)


  return this.http.post<any>(`${this.baseUrl}addProductReview`,data,httpOptions);
}
}



//   contactUs(data){
//     const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
   
    
//     return this.http.post<any>(`${this.baseUrl}contactUs`,data,);
//   }
//   getAllCard(){
//     const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//    var data={
//     "skip":0,
//     "limit":100
//    }
    
//     return this.http.post<any>(`${this.baseUrl}getAllCard`,data,httpOptions);
//   }

//   deleteCard(data){
//     const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
   
    
//     return this.http.post<any>(`${this.baseUrl}deleteCard`,data,httpOptions);
//   }
// // return this.http.get<any>(this.baseUrl + 'address?latitude=' + lat+ '&longitude=' + long );
// //   changePassword(data){
// //     const authorization =localStorage.getItem('token');
// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //   return this.http.put<any>(`${this.baseUrl}changepassword`, data,httpOptions);
// //   }
// //   /** Auth services */
// getUser(token){
//   const authorization =token;
//   console.log("authorization"+authorization);
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get(`${this.baseUrl}getUserProfile`,httpOptions);
// }


// phoneConfirmDialog(data) {
//   const dialogRef = this.dialog.open(PhoneConfirmComponent);
//   dialogRef.componentInstance.phone = data;
//   return dialogRef.afterClosed();
// }



// vehicleUpdateDialog(data) {
//   const dialogRef = this.dialog.open(VehicleUpdateModalComponent);
//   dialogRef.componentInstance.id = data;
//   return dialogRef.afterClosed();
// }


// setUpIntent(){
//   const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
   
    
//     return this.http.post<any>(`${this.baseUrl}setUpIntent`,httpOptions);
// }
// addCard(data){
//   const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
   
    
//     return this.http.post<any>(`${this.baseUrl}addCard`,data,httpOptions);

// }
// cancelBooking(data){
//   const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     var dataa={
//       "bookingId":data
//     }
    
//     return this.http.post<any>(`${this.baseUrl}cancelBooking`, dataa,httpOptions);
// }

// ratingAndReviewToDriver(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}ratingAndReviewToDriver`, data,httpOptions);
// }
// getVehicles(page){
//   var skip=(page==0?page:page-10);
//   var limit=10;
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get<any>(this.baseUrl + 'getVehicles?skip=' + skip+ '&limit=' + limit ,httpOptions);
// }

// getVehiclesAllWithoutPagination(){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get<any>(this.baseUrl + 'getVehicles' ,httpOptions);
// }

// getVehicleUpdate(id){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get<any>(this.baseUrl + 'getVehicles?_id=' + id,httpOptions);
// }

// getFare(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}createBookingPaymentCheck`, data,httpOptions);
// }

// createBook(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}createBooking`, data,httpOptions);
// }

// chargeWallet(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}chargeWallet`, data,httpOptions);
// }

// deleteVehicle(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}deleteVehicle`, data,httpOptions);
// }

// getVehicleType(token){
//   const authorization =token;
//   console.log("authorization"+authorization);
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get(`${this.baseUrl}getVehicleType`,httpOptions);
// }

// uploadFile(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//     return this.http.post<any>(`${this.baseUrl}uploadFile`, data,httpOptions);
// }

// addVehicle(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//     return this.http.post<any>(`${this.baseUrl}addVehicle`, data,httpOptions);
// }
// editVehicle(data){
//   const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//     return this.http.post<any>(`${this.baseUrl}editVehicle`, data,httpOptions);
// }

//   checkloginparams(obj: any) {
//     return this.http.post<any>(`${this.baseUrl}signUp`, obj);
//   }

//   checkForget(obj: any) {
//     return this.http.post<any>(`${this.baseUrl}signUpForgotPassword`, obj);
//   }
//   verifyOtp(otp){
//     return this.http.post<any>(`${this.baseUrl}verifyOtp`, otp);
//   }

//   verifyForgetOtp(otp){
//     return this.http.post<any>(`${this.baseUrl}verifyOtpForgotPassword`, otp);
//   }
  
//   completeRegistration(obj) {
//     return this.http.post<any>(`${this.baseUrl}complete`, obj);
//   }


//   getEventType(){
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get(`${this.baseUrl}getEventType`,httpOptions);

//   }

//   getChatMessages(data){
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}getChatMessages`, data,httpOptions);
//   }
//   getTeamType(){
//     var skip=0;
//     var limit=2000;
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.get(this.baseUrl + 'getTeam?limit=' + limit+ '&skip=' + skip ,httpOptions);

//   }
//   createEvent(data){
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}createEventBooking`, data,httpOptions);
//   }

//   editEvent(data){
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}editEventBooking`, data,httpOptions);
//   }

//   updateUser(data){
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//     return this.http.post<any>(`${this.baseUrl}updateUser`, data,httpOptions);
//   }

//   changePassword(data){
//     const authorization =localStorage.getItem('token');
//     const httpOptions = {
//      headers: new HttpHeaders({ 'authorization':authorization })
//   }
//   return this.http.post<any>(`${this.baseUrl}changePassword`, data,httpOptions);
//   }

//   signIn(obj) {
//     return this.http.post<any>(`${this.baseUrl}signIn`, obj);
//   }

//   verificationDialog() {
//     const dialogRef = this.dialog.open(VerificationModalAuthComponent, { disableClose: true });
//     return dialogRef.afterClosed();
//   }

//   vehicleDialog() {
//     const dialogRef = this.dialog.open(VehicleModalComponent, { disableClose: true });
//     return dialogRef.afterClosed();
//   }
//   changeForgetPassword(data){
   
//     return this.http.post<any>(`${this.baseUrl}forgotPassword`, data);
//     }

//     getAllBookingsEventsold(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }

//     var dataa={
//       "isUpcommingbookings":false,
//         "isPastbookings":true,
//         "isEventBooking":true,
//         "skip":(data==0?data:data-10),
//         "limit":10
//     }
    
//     return this.http.post<any>(`${this.baseUrl}getAllBooking`, dataa,httpOptions);
//     }


//     getAllNotification(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     var limit =10;

    
    
//     return this.http.get(this.baseUrl + 'getAllNotification?limit=' + limit+ '&skip=' + (data==0?data:data-10) ,httpOptions);

//     }
//     clearNotification(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     var dataa={
//       "_id":data
//     }
    
//     return this.http.post<any>(`${this.baseUrl}clearNotification`, dataa,httpOptions);
//     }

//     clearAllNotification(){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
   
    
//     return this.http.post<any>(`${this.baseUrl}clearAllNotification`,httpOptions);
//     }
//     getAllBookingsEventsnew(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     var dataa={
//       "isUpcommingbookings":false,
//         "isPastbookings":false,
//         "isEventBooking":true,
//         "skip":(data==0?data:data-10),
//         "limit":10
//     }
    
//     return this.http.post<any>(`${this.baseUrl}getAllBooking`, dataa,httpOptions);
//     }

//     getAllBookingsMainold(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }

//     var dataa={
//       "isUpcommingbookings":false,
//         "isPastbookings":true,
//         "isEventBooking":false,
//         "skip":(data==0?data:data-10),
//         "limit":10
//     }
    
//     return this.http.post<any>(`${this.baseUrl}getAllBooking`, dataa,httpOptions);
//     }

//     getServiceType(){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     return this.http.get(`${this.baseUrl}getServiceType`,httpOptions);
//     }

//     getBookingDetails(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     return this.http.post(`${this.baseUrl}getBookingDetails`,data,httpOptions);
//     }

//     cancelEventBooking(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     return this.http.post(`${this.baseUrl}cancelEventBooking`,data,httpOptions);
//     }

//     getAllBookingsMainnew(data){
//       const authorization =localStorage.getItem('token');
//       const httpOptions = {
//        headers: new HttpHeaders({ 'authorization':authorization })
//     }
//     var dataa={
//       "isUpcommingbookings":true,
//         "isPastbookings":false,
//         "isEventBooking":false,
//         "skip":(data==0?data:data-10),
//         "limit":10
//     }
//     return this.http.post<any>(`${this.baseUrl}getAllBooking`, dataa,httpOptions);
//     }

// //   updatecheck(data){
// //     console.log("data"+JSON.stringify(data));
// //     const authorization =localStorage.getItem('token');
// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //   return this.http.post<any>(`${this.baseUrl}updatecheck`, data,httpOptions);

// //   }

// //   changeForgetPassword(data){
   
// //   return this.http.put<any>(`${this.baseUrl}resetpassword`, data);
// //   }


// //   applyPrmoCode(data){
// //     const authorization =localStorage.getItem('token');

// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //     return this.http.post<any>(`${this.baseUrl}promocode`, data,httpOptions);
// //   }


// //   addAddress(data){
// //     const authorization =localStorage.getItem('token');

// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //     return this.http.post<any>(`${this.baseUrl}address`, data,httpOptions);
// //   }

// //   updateAddress(data){
// //     const authorization =localStorage.getItem('token');

// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //     return this.http.put<any>(`${this.baseUrl}address`, data,httpOptions);
// //   }

// //   getAddress(lat,long){
// //     const authorization =localStorage.getItem('token');

// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //   return this.http.get<any>(this.baseUrl + 'address?latitude=' + lat+ '&longitude=' + long );

// //   }

// //   getHome(data){
   
// //     return this.http.post<any>(`${this.baseUrlFood}`, data);
// //   }

// //   getBestOffer(data){
// //     // var latitude:number=0;
// //     // var longitude:number=0;
// //     //     if(localStorage.getItem('addresscooldash')){
      
// //     //       latitude=parseFloat(localStorage.getItem('lattcooldash'));
// //     //       longitude=parseFloat(localStorage.getItem('longgcooldash'));
// //     //       }
// //     // var userId=null;
// //     // if(localStorage.getItem("loggedInUser")){
// //     //   var user=JSON.parse(localStorage.getItem("loggedInUser"));
// //     //   var userdetails=user["_id"];
// //     //   return this.http.get<any>(this.baseUrlFood + 'allbestoffer?latitude=' + latitude + '&longitude=' + longitude + '&userId=' + userdetails);
      
// //     // }
    
// //     // else{
// //     //   var userdetails=null;
// //     //   return this.http.get<any>(this.baseUrlFood + 'allbestoffer?latitude=' + latitude+ '&longitude=' + longitude );
// //     // }

// //     return this.http.post<any>(`${this.baseUrlFood}allbestoffer`, data);
    
// //       }

// //   getRecommended(data){
// // // var latitude:number=0;
// // // var longitude:number=0;
// // //     if(localStorage.getItem('addresscooldash')){
  
// // //       latitude=parseFloat(localStorage.getItem('lattcooldash'));
// // //       longitude=parseFloat(localStorage.getItem('longgcooldash'));
// // //       }
// // // var userId=null;
// // // if(localStorage.getItem("loggedInUser")){
// // //   var user=JSON.parse(localStorage.getItem("loggedInUser"));
// // //   var userdetails=user["_id"];
// // //   return this.http.get<any>(this.baseUrlFood + 'allrecommened?latitude=' + latitude + '&longitude=' + longitude + '&userId=' + userdetails );
  
// // // }

// // // else{
// // //   var userdetails=null;
// // //   return this.http.get<any>(this.baseUrlFood + 'allrecommened?latitude=' + latitude+ '&longitude=' + longitude );
// // // }
// // return this.http.post<any>(`${this.baseUrlFood}allrecommened`, data);
// //   }


// //   getCategory(data){
    
// //     // var latitude:number=0;
// //     // var longitude:number=0;
// //     //     if(localStorage.getItem('addresscooldash')){
      
// //     //       latitude=parseFloat(localStorage.getItem('lattcooldash'));
// //     //       longitude=parseFloat(localStorage.getItem('longgcooldash'));
// //     //       }
// //     // var userId=null;
// //     // if(localStorage.getItem("loggedInUser")){
// //     //   var user=JSON.parse(localStorage.getItem("loggedInUser"));
// //     //   var userdetails=user["_id"];
// //     //   return this.http.get<any>(this.baseUrlFood + 'category?categoryId=' + categoryId + '&page=' + page + '&latitude=' + latitude  + '&longitude=' + longitude + '&userId=' + userdetails);
      
// //     // }
    
// //     // else{
// //     //   var userdetails=null;
// //     //   return this.http.get<any>(this.baseUrlFood + 'category?categoryId=' + categoryId + '&page=' + page  + '&latitude=' + latitude  + '&longitude=' + longitude );
// //     // }
// //     return this.http.post<any>(`${this.baseUrlFood}category`, data);
    
// //       }
// // getSearchResult(data){
// //   return this.http.post<any>(`${this.baseUrlFood}search`, data);
// // }

// //       getRestaurnatDetail(categoryId,outletId){
    
// //         var latitude:number=0;
// //         var longitude:number=0;
// //             if(localStorage.getItem('addresscooldash')){
          
// //               latitude=parseFloat(localStorage.getItem('lattcooldash'));
// //               longitude=parseFloat(localStorage.getItem('longgcooldash'));
// //               }
// //         var userId=null;
// //         if(localStorage.getItem("loggedInUser")){
// //           var user=JSON.parse(localStorage.getItem("loggedInUser"));
// //           var userdetails=user["_id"];
// //           if(outletId){
// //             return this.http.get<any>(this.baseUrlFood + 'detail/'+categoryId+'?outletId=' + outletId + '&latitude=' + latitude  + '&longitude=' + longitude + '&userId=' + userdetails);
// //           }
// //           else{
// //             return this.http.get<any>(this.baseUrlFood + 'detail/'+categoryId+'?latitude=' + latitude  + '&longitude=' + longitude + '&userId=' + userdetails);
// //           }
         
          
// //         }
        
// //         else{
// //           var userdetails=null;
// //           if(outletId){
// //             return this.http.get<any>(this.baseUrlFood + 'detail/'+categoryId+'?outletId=' + outletId + '&latitude=' + latitude  + '&longitude=' + longitude );
// //           }
// //           else{
// //             return this.http.get<any>(this.baseUrlFood + 'detail/'+categoryId+'?latitude=' + latitude  + '&longitude=' + longitude );
// //           }
          
// //         }
        
// //           }

// //   getSavedRes(data){
// //     // var latitude:number=0;
// //     // var longitude:number=0;
// //     //     if(localStorage.getItem('addresscooldash')){
      
// //     //       latitude=parseFloat(localStorage.getItem('lattcooldash'));
// //     //       longitude=parseFloat(localStorage.getItem('longgcooldash'));
// //     //       }
// //     // var userId=null;
// //     // if(localStorage.getItem("loggedInUser")){
// //     //   var user=JSON.parse(localStorage.getItem("loggedInUser"));
// //     //   var userdetails=user["_id"];
// //     //   return this.http.get<any>(this.baseUrlFood + 'allsaved?latitude=' + latitude + '&longitude=' + longitude + '&userId=' +userdetails);
      
// //     // }
    
// //     // else{
// //     //   var userdetails=null;
// //     //   return this.http.get<any>(this.baseUrlFood + 'allsaved?latitude=' + latitude+ '&longitude=' + longitude );
// //     // }
// //     return this.http.post<any>(`${this.baseUrlFood}allsaved`, data);
    
// //       }



// //       addressDialog(data) {
// //         const dialogRef = this.dialog.open(OutletModalComponent);
// //         dialogRef.componentInstance.address = data;
// //         return dialogRef.afterClosed();
// //       }

// //       placeOrder(data){
// //         return this.http.post<any>(`${this.baseUrlFood}order`, data);
// //       }
// //    getAllOrders(status,page){
// //     if(localStorage.getItem("loggedInUser")){
// //         var user=JSON.parse(localStorage.getItem("loggedInUser"));
// //         var userdetails=user["_id"];
// //         return this.http.get<any>(this.baseUrlFood + 'order?status=' + status+ '&page=' + page + '&userId=' + userdetails);
// //     }
    
// //    }



// //    getNotifications(status,page){
// //     // if(localStorage.getItem("loggedInUser")){
// //     //     var user=JSON.parse(localStorage.getItem("loggedInUser"));
// //     //     var userdetails=user["_id"];
// // var loc=0;
// //     const authorization =localStorage.getItem('token');

// //     const httpOptions = {
// //      headers: new HttpHeaders({ 'authorization':authorization })
// //   }
// //         return this.http.get<any>(this.baseUrl + 'notification?verticalType=' + loc+ '&page=' + page ,httpOptions);
// //     // }
    
// //    }
   
   
// //    addFavourite(data){
// //     return this.http.post<any>(`${this.baseUrlFood}favourite`, data);
// //    }
   
// //    getOrderdetails(data){
// //     return this.http.get<any>(this.adminUrl + 'order/' + data);
// //    }

// //    getHeaderSearchCategory(data){
// //     return this.http.post<any>(`${this.baseUrlFood}search`, data); 
// //    }
// //    socialLogin(data){
// //     return this.http.post<any>(`${this.baseUrl}sociallogin`, data); 
// //    }

// //    socialRegister(data){
// //     return this.http.post<any>(`${this.baseUrl}socialregister`, data); 
// //    }



