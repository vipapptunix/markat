import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { CommonService } from "./common.service";
import { Router } from "@angular/router";

@Injectable({ providedIn: "root" })
export class AuthResponseInterceptor implements HttpInterceptor {
  token = "";
  viewIs;
  constructor(private commonService: CommonService, private router: Router) {
    router.events.subscribe((val) => {

      this.viewIs = window.location.href;
      var lastSlash = window.location.href.lastIndexOf('/');
      this.viewIs = this.viewIs.substr(lastSlash + 1);

    });
  }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem("confirmauth");

    if (token) {
      req = req.clone({ headers: req.headers.set("Authorization", token) });
    }
    req = req.clone({ headers: req.headers.set("Accept", "application/json") });

    return next.handle(req).pipe(catchError(err => this.handleError(err)));
  }

  private handleError(err: HttpErrorResponse): Observable<any> {

    console.log("Eroorrrrr handler" + JSON.stringify(err));
    let errorMsg;
    if (err.error instanceof Error) {

      errorMsg = `Error: Unable to connect`;
    } else {
      errorMsg = `Error: ${err.status}, Unable to connect`;
    }

    if (err.error && (err.status == 401)) {
     // this.commonService.showToasterError('Un-authorized ! Please login first.')

     // this.router.navigate(["/home"]);

   //   this.clearStorage();

    }

    else if (err.status == 400) {
      console.log("Eroorrrrr handler 400" + JSON.stringify(err));
      this.commonService.showToasterError(err.error.message);
      // this.clearStorage();
      // if(this.viewIs=="home"){
      //   this.router.navigate(["/"]);
      // }
      // else{
      //   this.router.navigate(["/home"]);
      // }
    }


    else if (err.status == 403) {
      this.commonService.showToasterError('Please sign-in Again.')
      this.clearStorage();
      if (this.viewIs == "home") {
       // this.router.navigate(["/"]);
      }
      else {
        this.router.navigate(["/home"]);
      }
    } else if (err.error && err.status == 500 && !err.error.success) 
    {
     // this.commonService.showToasterError('Something is wrong !');
    } else if (err.error && !err.error.success) 
    {
      // this.commonService.showToasterError('Unable to connect, please wait and try to sign-in again.');
     // this.clearStorage();
      if (this.viewIs == "home") {
        //this.router.navigate(["/"]);
      }
      else {
        this.router.navigate(["/home"]);
      }
      // this.router.navigate(['/']);
    } else {
      if (errorMsg)
        this.commonService.showToasterError(errorMsg)
    }
    return errorMsg;
  }

  clearStorage() {
    localStorage.removeItem("loggedInUser");
   // localStorage.removeItem("confirmauth");
    localStorage.removeItem("verify");
    localStorage.removeItem("changePassword");
    localStorage.removeItem('addresscooldash')
    localStorage.removeItem('ItemsAdded');
    localStorage.removeItem('lattcooldash');
    localStorage.removeItem('longgcooldash');
    localStorage.removeItem('socialuser');
    localStorage.removeItem('fblogin');
    localStorage.removeItem('authTokenn');
    localStorage.removeItem('signUpSocial');
    localStorage.removeItem('trackorder');
    localStorage.removeItem('trackstatus');

  }
}
